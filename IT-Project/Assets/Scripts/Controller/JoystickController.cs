﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Character controller with joystick.
/// </summary>
public class JoystickController : MonoBehaviour 
{
	[HideInInspector]
	public VirtualJoystick joystick;

	public float baseVel = 200.0f;
	private int scalar = 50;
	public float inputDelay = 0.1f;
	public float forwardVel = 10.0f;
	public float rotateVel = 100.0f;
	public bool reverse = true;
	public float drag = 0.1f;
	public float normalWalkingRate = 1.0f;	// Rate for the walking speed
	public float attackWalkingRate = 0.8f;	// Rate for the walking speed while attacking

	protected Vector3 moveVector{ set; get; }
	protected Quaternion targetRotation;
	protected Rigidbody rBody;
	protected Animation anim;
	protected Character actor;

	void Start ()
	{
		// Gets Rigidbody object.
		if ((rBody = GetComponent<Rigidbody> ()) == null)
			Debug.LogError ("The character needs a rigid body.");

		// Gets Animation object.
		if ((anim = GetComponent<Animation> ()) == null)
			Debug.LogError ("The character needs an Animator.");

		// Gets Actor object.
		if ((actor = GetComponent<Character> ()) == null)
			Debug.LogError ("The character needs to be an actor.");

		if ((joystick = GameObject.Find ("JoystickBackgroundImage").GetComponent <VirtualJoystick> ()) == null)
			Debug.LogError ("The VirtualJoystick could not be found in Controller");

		targetRotation = transform.rotation;
		rBody.drag = drag;
	}

	// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
	// FixedUpdate should be used instead of Update when dealing with Rigidbody
	public void FixedUpdate ()
	{
		if (actor.IsControllable ()) 
		{
			GetInput ();
			Turn ();
			Walk ();

			if (Input.GetKeyDown (KeyCode.F))
				actor.weapon.Attack ();
		}
		else
			// Character needs to stay at current place.
			rBody.velocity = Vector3.zero;
	}

	private void GetInput ()
	{
		Vector3 tmpVector = Vector3.zero;
		tmpVector = Vector3.zero;
		tmpVector.x = joystick.Horizontal ();
		tmpVector.z = joystick.Vertical ();
		//Debug.Log ("magnitude " + tmpVector.magnitude + ", x " +tmpVector.x + ", z " +tmpVector.z);
		if (tmpVector.magnitude > 1)
			tmpVector.Normalize ();

		moveVector = tmpVector;
	}

	/// <summary>
	/// Lets the character walk in response to the joystick input.
	/// </summary>
	public void Walk ()
	{
		// Too small input will be ignored.
		if (Mathf.Abs (moveVector.magnitude) > inputDelay && actor.CanWalk ()) 
		{
			// Used to adjust the walking speed.
			float walkingRate = normalWalkingRate;

			// Prevents switching to the walk animation while the player is in other motion.
			if (!actor.anim.IsPlaying ("Attack")) 
				anim.Play ("Walk");
			else
				// Slows down character's movement while it's attacking.
				walkingRate = attackWalkingRate;

			setVelocity (walkingRate);

		} else {

			rBody.velocity = Vector3.zero;

			// Prevents switching to the wait animation while the player is in other motion.
			if (actor.CanWait () ) 
				anim.Play ("Wait");
		}	
	}

	// Calculates the velocity.
	private void setVelocity (float slowRate)
	{
		Vector3 tmpVector = transform.forward.normalized * baseVel;
		rBody.velocity = tmpVector + transform.forward.normalized * moveVector.magnitude * forwardVel * slowRate * scalar;
	}

	/// <summary>
	/// Lets the character turn in response to the joystick input.
	/// </summary>
	public void Turn()
	{
		// Ignore minute inputs.
		if (Mathf.Abs (moveVector.magnitude) > inputDelay && actor.CanTurn ()) 
		{
			// Allighs the character's movement to the joystick angle.
			moveVector = Quaternion.Euler (0, 135, 0) * moveVector;
			//transform.forward = Vector3.Normalize (new Vector3 (moveVector.x, 0.0f, moveVector.z ));
			Vector3 dir = Vector3.RotateTowards(transform.forward, moveVector, moveVector.magnitude * rotateVel * scalar, 0.0f); 
			dir.y = 0;
			transform.rotation = Quaternion.LookRotation (dir);
		}
	}
}