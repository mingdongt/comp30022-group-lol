﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Defines the behaviour of action button. 
/// </summary>
public class SkillButton1 : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public Player player;
    public SkillBoom skill;

    public Image cover;
    public int coolDown;
    private float timePassed;

    void Start()
    {
		if (player == null)
            Debug.Log ("The attack button needs an actor.");

		if (GameManager.Instance.singleplay && (skill = player.GetComponent <SkillBoom>()) == null)
            Debug.LogError ("The character needs have a skill.");
	
        timePassed = coolDown;
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {

        if (skill == null)
            player.GetComponent<SkillBoom>();

        if (!(timePassed < coolDown) && !player.GetComponent<Player>().IsDown()) {
            skill.cast();
            if (!GameManager.Instance.singleplay)
                player.netPlayer.SkillPRC();
            timePassed = 0;
        }

    }

    void Update()
    {
        if (timePassed < coolDown)
        {
            timePassed += Time.deltaTime;
            cover.fillAmount = timePassed / coolDown;
        }
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        // Currently nothing is done here.
    }

}
