﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

/// <summary>
/// only for singleplayer,
/// Pausing the game sets the progression of time to 0 i.e. pauses the game on being pressed,
/// when paused, ui elements are disabled, and the pause button background is lit
/// also handles applying the volume sliders' effects
/// </summary>
public class PauseButton : MonoBehaviour{


	private Color pressedColor = new Color(1f, 1f, 1f, 1f);
	private Color clearColor = new Color(1f, 1f, 1f, 0f);
	public bool paused = false;
    public bool pressed = false;
    float bSize = 40f;

    public Texture bImage;
    public Texture box;
    public GameObject attackButton;
    public GameObject skillButton1;
    public GameObject potionButton;
    public GameObject joystick;
    public GameObject hpBar;
    public AudioSource bgmSource;

    public float sfxSliderValue;
    public float bgmSliderValue;

    //location of pause UI elements in relation to screen size
    Rect menuBG = new Rect(Screen.width * 0.3f, Screen.height * 0.3f, Screen.width * 0.4f, Screen.height * 0.4f);
    Rect sfxLabel = new Rect(Screen.width * 0.35f, Screen.height * 0.335f, Screen.width * 0.1f, Screen.height * 0.1f);
    Rect sfxSlider = new Rect(Screen.width * 0.45f, Screen.height * 0.35f, Screen.width * 0.2f, Screen.height * 0.05f);
    Rect bgmLabel = new Rect(Screen.width * 0.35f, Screen.height * 0.485f, Screen.width * 0.1f, Screen.height * 0.1f);
    Rect bgmSlider = new Rect(Screen.width * 0.45f, Screen.height * 0.5f, Screen.width * 0.2f, Screen.height * 0.05f);
    Rect button1 = new Rect(Screen.width * 0.35f, Screen.height * 0.575f, Screen.width * 0.125f, Screen.height * 0.1f);
    Rect button2 = new Rect(Screen.width * 0.525f, Screen.height * 0.575f, Screen.width * 0.125f, Screen.height * 0.1f);

    void Awake() {
        sfxSliderValue = AudioListener.volume;
        bgmSliderValue = PlayerPrefs.GetFloat("BGM");
        bgmSource.volume = bgmSliderValue;

        bSize = Screen.height * 0.1f;
    }

    void OnGUI() {

        if (pressed) {
			GUI.depth = 1;
            GUI.backgroundColor = pressedColor;
            GUI.DrawTexture(menuBG, box, ScaleMode.StretchToFill);

            sfxSliderValue = GUI.HorizontalSlider(sfxSlider, sfxSliderValue, 0.0f, 1.0f);

            GUIStyle labelStyle = new GUIStyle(GUI.skin.GetStyle("label"));
            labelStyle.normal.textColor = Color.black;
            labelStyle.fontSize = 32;
            GUI.Label(sfxLabel, "SFX", labelStyle);

            bgmSliderValue = GUI.HorizontalSlider(bgmSlider, bgmSliderValue, 0.0f, 1.0f);
            GUI.Label(bgmLabel, "Music", labelStyle);
            //audio
            AudioListener.volume = sfxSliderValue;
            bgmSource.volume = bgmSliderValue;
            PlayerPrefs.SetFloat("Volume", sfxSliderValue);
            PlayerPrefs.SetFloat("BGM", bgmSliderValue);
            PlayerPrefs.Save();
            //

            GUIStyle buttonStyle = new GUIStyle(GUI.skin.button);
            buttonStyle.fontSize = 32;

            if (GUI.Button(button1,"Save Game", buttonStyle)) {
                GameObject.Find("SaveLoadHost").GetComponent<SaveLoad>().SaveSaveState();
            }

            if (GUI.Button(button2, "Exit", buttonStyle)) {
                if (GameManager.Instance.singleplay) {
                    pause();
                    
                }
                
				GameManager.Instance.Restart ();
            }
        }
        else {
            GUI.backgroundColor = clearColor;
        }
        
		
        if(GUI.Button(new Rect(Screen.width -bSize, 0,bSize,bSize),bImage)) {
            pressed = !pressed;
            if (GameManager.Instance.singleplay) {
                pause();
            }
        }
    }

    /// <summary>
    /// uses GameObject's active functionality to disable them,
    /// relies on having references in the unity inspector
    /// </summary>
    public void pause() {
        if (!paused) {
			Time.timeScale = 0;
		    paused = true;
            attackButton.SetActive(false);
            skillButton1.SetActive(false);
            potionButton.SetActive(false);
            joystick.SetActive(false);
            hpBar.SetActive(false); 
 

        } else {
			Time.timeScale = 1;
			paused = false;
            attackButton.SetActive(true);
            skillButton1.SetActive(true);
            potionButton.SetActive(true);
            joystick.SetActive(true);
            hpBar.SetActive(true);


        }
    }
}
