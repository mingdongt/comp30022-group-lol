﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

/// <summary>
/// Defines the behaviour of action button. 
/// </summary>
public class AttackButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler  {

	public Player player;

	private void Start ()
	{

		if (player == null)
			Debug.Log ("The attack button needs an actor.");

	}

	public virtual void OnPointerDown (PointerEventData ped)
	{

		player.weapon.Attack ();
	}

	public virtual void OnPointerUp (PointerEventData ped)
	{
		// Currently nothing is done here.
	}

}
