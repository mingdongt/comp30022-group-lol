﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

/// <summary>
/// Represents the virtual joystick. Handles drag, down, and up events.
/// </summary>
public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler 
{
	public float maxRadius = 3.5f; // Determines the radius within which joystick can move around. 

	private Image backgroundImg;
	private Image joystickImg;
	private Vector3 inputVector;

	private void Start ()
	{
		if ((backgroundImg = GetComponent<Image> ()) == null)
			Debug.LogError ("BackgroundImg needs to be set.");
		
		if ((joystickImg = transform.GetChild (0).GetComponent<Image> ()) == null)
			Debug.LogError ("JoystickImg needs to be set.");
	}

	public virtual void OnPointerDown (PointerEventData ped)
	{

		OnDrag (ped);
	}

	public virtual void OnPointerUp (PointerEventData ped)
	{
		inputVector = Vector3.zero;
		joystickImg.rectTransform.anchoredPosition = Vector3.zero;
	}

	public float Horizontal ()
	{
		return inputVector.x;
	}

	public float Vertical ()
	{
		return inputVector.z;
	}

	// For developping. To be removed. 
	public float HorizontalWithKeyboard ()
	{
		if (inputVector.x != 0)
			return inputVector.x;
		else
			return Input.GetAxis ("Horizontal");
	}

	// For developping. To be removed. 
	public float VerticalWithKeyboard ()
	{
		if (inputVector.z != 0)
			return inputVector.z;
		else
			return Input.GetAxis ("Vertical");
	}


	public void OnDrag (PointerEventData ped)
	{

        Vector2 pos;
        // Checking whether the input was made within the joystick graphics.
        // Not exact since it checks the rectangle area while the joystick graphics is a circle.
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(backgroundImg.rectTransform, ped.position, ped.pressEventCamera, out pos)) {
            pos.x = (pos.x / backgroundImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / backgroundImg.rectTransform.sizeDelta.y);

            // Normalising the coordinate for convenience.
            //inputVector = new Vector3 (pos.x * 2 + 1, 0, pos.y * 2 - 1); 
            inputVector = new Vector3(pos.x, 0.0f, pos.y);
            if (inputVector.magnitude > 1.0f)
               inputVector = inputVector.normalized;

            // Move joystick image to the neutral position after releasing the joypad.
            // Note: Vector3 can be initialised with two parameters.
            joystickImg.rectTransform.anchoredPosition = new Vector3(inputVector.x * (backgroundImg.rectTransform.sizeDelta.x / maxRadius),
            inputVector.z * (backgroundImg.rectTransform.sizeDelta.y / maxRadius));

            //Debug.Log (inputVector);
        }

	}
}
