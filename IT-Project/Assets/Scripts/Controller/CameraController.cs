﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public Transform target;
	public float lookSmooth = 0.09f;
	public Vector3 offsetFromTarget = new Vector3 (0, 6, -8);

	Vector3 destination = Vector3.zero;
	//CharacterControllerWithKeyboard charController;
	float rotateVel = 0.0f;

	void Start ()
	{
		SetCameraTarget (target);
	}

	void SetCameraTarget (Transform transform)
	{
		if (target != null)
			target = transform;
	}

	// LateUpdate is called every frame, if the Behaviour is enabled.
	void LateUpdate()
	{
		MoveToTargetWithoutRotation();
	}

	// Lets the camera follow without rotating itself.
	void MoveToTargetWithoutRotation()
	{
		if (target == null)
			return;
		
		destination = offsetFromTarget;
		destination += target.position;
		transform.position = destination;
	}

	// Lets the camera rotate so that it looks at the player.
	void LookAtTarget()
	{
		if (target == null)
			return;
		
		// Calculates the degree of angle to rotate
		float eulerYAngle = Mathf.SmoothDampAngle (transform.eulerAngles.y, target.eulerAngles.y, ref rotateVel, lookSmooth);
		transform.rotation = Quaternion.Euler (transform.eulerAngles.x, eulerYAngle, 0);
	}



}
