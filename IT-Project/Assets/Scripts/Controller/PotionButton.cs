﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Defines the behaviour of action button. 
/// </summary>
public class PotionButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public Player player;
    public UsePotion potion;

    public Image cover;
    public int coolDown;
    private float timePassed;


    void Start()
    {

		if (player == null)
            Debug.Log ("The attack button needs an actor.");

		if (GameManager.Instance.singleplay && (potion = player.GetComponent<UsePotion>()) == null)
			Debug.Log ("The character needs have a potion.");

        timePassed = coolDown;

    }

    public virtual void OnPointerDown(PointerEventData ped)
    {

        if (potion == null)
            player.GetComponent<UsePotion>();

        if (!(timePassed < coolDown) && !player.GetComponent<Player>().IsDown()) {
            potion.Use();
            player.healthSlider.value = player.healthPoint;
            if (!GameManager.Instance.singleplay)
                player.netPlayer.PotionRPC();
            timePassed = 0;
        }

    }

    void Update()
    {
        if (timePassed < coolDown)
        {
            timePassed += Time.deltaTime;
            cover.fillAmount = timePassed / coolDown;
        }
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        // Currently nothing is done here.
    }

}
