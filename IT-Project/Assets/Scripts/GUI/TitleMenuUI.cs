﻿using UnityEngine;
using System.Collections;

/// <summary>
/// In charge of drawing the Title Screen UI elements.
/// Also handles the application of sound levels from the playerprefs.
/// </summary>
public class TitleMenuUI : MonoBehaviour {
    
    public Texture bgImage;
    public Texture panel;

    public GameManager gameManager;
    public AudioSource bgmSource;

    //location of each element of the UI, as a relative position to the screen size
    Rect bgRect;
    Rect menuRect;

    Rect singleRect;
    Rect contRect;
    Rect replayRect;
    Rect multiRect;
    Rect invenRect;
    Rect storeRect;
    Rect settingRect;
    Rect statsRect;

    Rect panelRect;

    Rect sfxSliderRect;
    Rect sfxLabelRect;
    Rect bgmSliderRect;
    Rect bgmLabelRect;

    private bool showSettings = false;
    private float sfxValue;
    private float bgmValue;

    void Awake() {
        bgRect = new Rect(0, 0, Screen.width, Screen.height);
        menuRect = new Rect(Screen.width * 0.53f, Screen.height * 0.08f, Screen.width * 0.39f, Screen.height * 0.84f);

        singleRect = new Rect(Screen.width * 0.55f, Screen.height * 0.1f, Screen.width * 0.35f, Screen.height * 0.14f);
        contRect = new Rect(Screen.width * 0.55f, Screen.height * 0.26f, Screen.width * 0.17f, Screen.height * 0.14f);
        replayRect = new Rect(Screen.width * 0.73f, Screen.height * 0.26f, Screen.width * 0.17f, Screen.height * 0.14f);
        multiRect = new Rect(Screen.width * 0.55f, Screen.height * 0.42f, Screen.width * 0.35f, Screen.height * 0.14f);
        invenRect = new Rect(Screen.width * 0.55f, Screen.height * 0.58f, Screen.width * 0.17f, Screen.height * 0.14f);
        storeRect = new Rect(Screen.width * 0.73f, Screen.height * 0.58f, Screen.width * 0.17f, Screen.height * 0.14f);
        settingRect = new Rect(Screen.width * 0.55f, Screen.height * 0.74f, Screen.width * 0.17f, Screen.height * 0.14f);
        statsRect = new Rect(Screen.width * 0.73f, Screen.height * 0.74f, Screen.width * 0.17f, Screen.height * 0.14f);

        panelRect = new Rect(Screen.width * 0.1f, Screen.height * 0.1f, Screen.width * 0.35f, Screen.height * 0.78f);

        sfxSliderRect = new Rect(Screen.width * 0.25f, Screen.height * 0.15f, Screen.width * 0.15f, Screen.height * 0.05f);
        sfxLabelRect = new Rect(Screen.width * 0.125f, Screen.height * 0.14f, Screen.width * 0.1f, Screen.height * 0.1f);
        bgmSliderRect = new Rect(Screen.width * 0.25f, Screen.height * 0.3f, Screen.width * 0.15f, Screen.height * 0.05f);
        bgmLabelRect = new Rect(Screen.width * 0.125f, Screen.height * 0.29f, Screen.width * 0.1f, Screen.height * 0.1f);

        gameManager = GameObject.Find("_Manager").GetComponent<GameManager>();

        //getting and setting the sound levels
        if (PlayerPrefs.HasKey("Volume")) {
            AudioListener.volume = PlayerPrefs.GetFloat("Volume");
        }
        else {
            PlayerPrefs.SetFloat("Volume", AudioListener.volume);
        }

        if(PlayerPrefs.HasKey("BGM")) {
            bgmValue = PlayerPrefs.GetFloat("BGM");
            bgmSource.volume = bgmValue;
        } else {
            PlayerPrefs.SetFloat("BGM", 1.0f);
            bgmValue = 1.0f;
        }
        sfxValue = AudioListener.volume;
    }

    void OnGUI() {
		GUI.depth = 2;
		GUIStyle buttonStyle = new GUIStyle (GUI.skin.button);
		buttonStyle.fontSize = Screen.width/40;

		GUI.DrawTexture (bgRect, bgImage, ScaleMode.StretchToFill);

		// Shows login screen at the game start. 
		if (!GUIManager.IsLoginGUIEnabled ())  {

			//main buttons, to the right
			GUI.DrawTexture (menuRect, panel, ScaleMode.StretchToFill);

			if (GUI.Button (singleRect, "Singleplayer", buttonStyle)) {
				gameManager.StartSinglePlay ();
			}
            if (GUI.Button (contRect, "Continue", buttonStyle)) {
                gameManager.isContinue = true;
                gameManager.StartSinglePlay();
			}
			if (GUI.Button (replayRect, "Replay", buttonStyle)) {
                Application.LoadLevel ("ReplayList");

			}
			if (GameManager.Instance.IsLoggedIn) {
				if (GUI.Button (multiRect, "Multiplayer", buttonStyle)) {
					gameManager.StartMultiPlay ();
				}
                if (GUI.Button (invenRect, "Inventory", buttonStyle)) {
                    gameManager.StartInventory();
			    }
                if (GUI.Button(storeRect, "Store", buttonStyle))
                {
                    gameManager.StartStore();
                }
            } else { 
				// Not logged in, so Multiplayer mode, inventory and store are not available.
				GUI.color = Color.gray;
				GUI.Box (multiRect, "Multiplayer", buttonStyle);
                GUI.Box(invenRect, "Inventory", buttonStyle);
                GUI.Box(storeRect, "Store", buttonStyle);
                GUI.color = Color.white;
			}
		
			if (GUI.Button (settingRect, "Settings", buttonStyle)) {
				showSettings = !(showSettings);
			}
			if (GUI.Button (statsRect, "Stats", buttonStyle)) {

			}
			//settings panel
			if (showSettings) {

				GUI.DrawTexture (panelRect, panel, ScaleMode.StretchToFill);
				sfxValue = GUI.HorizontalSlider (sfxSliderRect, sfxValue, 0.0f, 1.0f);

				GUIStyle labelStyle = new GUIStyle (GUI.skin.GetStyle ("label"));
				labelStyle.normal.textColor = Color.black;
				labelStyle.fontSize = 32;
				GUI.Label (sfxLabelRect, "SFX", labelStyle);

				bgmValue = GUI.HorizontalSlider (bgmSliderRect, bgmValue, 0.0f, 1.0f);
				GUI.Label (bgmLabelRect, "Music", labelStyle);

				AudioListener.volume = sfxValue;
				bgmSource.volume = bgmValue;
				PlayerPrefs.SetFloat ("Volume", sfxValue);
				PlayerPrefs.SetFloat ("BGM", bgmValue);
				PlayerPrefs.Save ();

			}

			// Displays error/warning messages.
			GUIStyle warningStyle = new GUIStyle (GUI.skin.label);
			warningStyle.normal.textColor = Color.red;
			warningStyle.fontSize = 30;
			if (!GameManager.Instance.IsLoggedIn)
				GUI.Label (new Rect (30 , Screen.height - 60, 300, 40), "You are not logged in",
					warningStyle);
			else if (!GameManager.Instance.IsLoggedIn) 
				GUILayout.Label ("ERROR: input was empty", GUILayout.Width (300));
			else
				GUILayout.Label ("", GUILayout.Width (300));
			
		} else
			// Displays login screen.
			GUIManager.ShowLoginScreen ();
    }
}
