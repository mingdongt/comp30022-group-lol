﻿using UnityEngine;
using System.Collections;

/// <summary>
/// GUI manager. Responsible for organising GUI components of the game.
/// It works as a helper class which draws GUI on behalf of other classes.
/// </summary>
public class GUIManager : MonoBehaviour 
{
	private static bool isLobbyGUIEnabled;	// Used as a switch to turn on/off the lobby GUI.
	private static bool isLoginGUIEnabled; // Used to indicate the login status.

	void Start ()
	{
		if (GameManager.Instance.IsLoggedIn)
			isLoginGUIEnabled = false;
		else
			isLoginGUIEnabled = true;
		
		isLobbyGUIEnabled = false;
	}

	/// <summary>
	/// Returns if the lobby GUI is enabled.
	/// </summary>
	/// <returns><c>true</c> if lobby GUI enabled; otherwise, <c>false</c>.</returns>
	public static bool IsLobbyGUIEnabled ()
	{
		return isLobbyGUIEnabled;
	}

	/// <summary>
	/// Returns if the Login GUI is enabled.
	/// </summary>
	/// <returns><c>true</c> if Login GUI enabled; otherwise, <c>false</c>.</returns>
	public static bool IsLoginGUIEnabled ()
	{
		return isLoginGUIEnabled;
	}

	/// <summary>
	/// Displays Lobby GUI where players can create a new game session or
	/// join in an existing game session.
	/// </summary>
	public static void ShowLobby ()
	{
		isLobbyGUIEnabled = true;
	}

	/// <summary>
	/// Closes Lobby GUI.
	/// </summary>
	public static void CloseLobby ()
	{
		isLobbyGUIEnabled = false;
		GameObject.Find ("GUIHost").GetComponent <Lobby> ().enabled = false;
	}
    
	/// <summary>
	/// Displays the login screen.
	/// When login button is pressed and the login was successful, Lobby will be displayed.
	/// Error and/or warning message will be displayed when they are appropriate.
	/// </summary>
	public static void ShowLoginScreen ()
	{
		isLoginGUIEnabled = true;
	}

	/// <summary>
	/// Closes the login screen.
	/// </summary>
	public static void CloseLoginScreen ()
	{
		isLoginGUIEnabled = false;
		GameObject.Find ("GUIHost").GetComponent <Login> ().enabled = false;
	}

	/// <summary>
	/// Used to insert start horizontal/vertical.
	/// </summary>
	public static void InsertStartSpace ()
	{
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		GUILayout.BeginVertical ();
		GUILayout.FlexibleSpace ();
	}
		
	/// <summary>
	/// Used to insert end horizontal/vertical.
	/// </summary>
	public static void InsertEndSpace ()
	{
		GUILayout.EndHorizontal ();
		GUILayout.FlexibleSpace ();
		GUILayout.EndVertical ();
		GUILayout.FlexibleSpace ();
	}
}
