﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class provides the Lobby GUI and the lobby functionality
/// which allows a player to create or select a game session in which 
/// the player can play the multiplayer mode.
/// </summary>
public class Lobby : MonoBehaviour 
{
	private static Vector2 scrollPositionForLobby; // Used for the scroll in the lobby GUI.
	private static int gapOnTopOfLobby = Screen.height/5; // Gap that is placed into the top of Lobby GUI.


	// Used as params for lobby GUI functions.
	static int maxPlayer;
	static int defalutNumPlayer = 5;
	static string roomName = "room name";
	static string userName = "user name";
	static string maxPlayerString= "5";

	private static int btnFontSize = 24; // Font size for login button text.
	private static int labelFontSize = 24; // Font size for login label text.
	private static int boxFontSize = 32; // Font size for warning message in login screen.
	private static int inputFontSize = 24; // Font size for warning message in login screen.

	// Sizes for GUI components.
	private static int btnWidth = Screen.width/4;
	private static int btnHeight = Screen.height/10;
	private static int labelWidth = Screen.width/3;
	private static int gapSize = Screen.width/80;

	void OnGUI ()
	{
		if (!GUIManager.IsLobbyGUIEnabled ())
			return;

		// Style for button.
		GUIStyle lobbyStyle = new GUIStyle (GUI.skin.label);
		lobbyStyle.fontSize = boxFontSize;
		lobbyStyle.normal.textColor = Color.red;

		// Session style for button.
		GUIStyle sessionStyle = new GUIStyle (GUI.skin.box);
		sessionStyle.fontSize = labelFontSize;
		sessionStyle.normal.textColor = Color.green;

		// Style for button.
		GUIStyle btnStyle = new GUIStyle (GUI.skin.button);
		btnStyle.fontSize = btnFontSize;
		btnStyle.normal.textColor = Color.white;

		// Style for label.
		GUIStyle labelStyle = new GUIStyle (GUI.skin.label);
		labelStyle.fontSize = labelFontSize;
		labelStyle.fontStyle = FontStyle.Italic;
		labelStyle.normal.textColor = Color.white;

		// Style for input text.
		GUIStyle inputStyle = new GUIStyle (GUI.skin.textField);
		inputStyle.normal.textColor = Color.grey;
		inputStyle.fontStyle = FontStyle.Italic;
		inputStyle.fontSize = inputFontSize;

		GUI.depth = 2;
		GUI.skin.label.alignment = TextAnchor.MiddleCenter;	

		GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
		GUI.color = Color.white;
		GUILayout.BeginArea (new Rect (0, gapOnTopOfLobby, Screen.width, Screen.height - gapOnTopOfLobby));

		GUIManager.InsertStartSpace ();
		// Title.
		GUILayout.Box ("Lobby", lobbyStyle, GUILayout.Height (boxFontSize+gapSize), GUILayout.Width (labelWidth*2));	

		// Username input textbox.
		GUI.color = Color.green;
		GUILayout.Label ("You are: " + GameManager.Instance.GetPlayerName (), labelStyle, 
			GUILayout.Height (inputFontSize+gapSize/2), GUILayout.Width (labelWidth*2));


		GUILayout.Space (gapSize);
		GUILayout.BeginHorizontal ();
		GUILayout.BeginVertical ();

		// Room name input textbox.
		GUI.color = Color.white;
		GUILayout.Label ("Session Name:", labelStyle, 
			GUILayout.Height (inputFontSize+gapSize), GUILayout.Width (labelWidth-gapSize));
		
		roomName = GUILayout.TextField (roomName, inputStyle,
			GUILayout.Height (inputFontSize+gapSize), GUILayout.Width (labelWidth-gapSize));

		// Max player input textbox.
		GUILayout.Label ("Max # of players 1 - " + GameManager.Instance.NetManager.maxPlayer + ": ", 
			labelStyle, GUILayout.Height (inputFontSize+gapSize), GUILayout.Width (labelWidth-gapSize));
		
		maxPlayerString = GUILayout.TextField (maxPlayerString, defalutNumPlayer, inputStyle,
			GUILayout.Height (inputFontSize+gapSize), GUILayout.Width (labelWidth-gapSize));
		
		GUILayout.EndVertical ();
		GUILayout.Space (gapSize*5);
		GUI.color = Color.white;

		if (maxPlayerString != "") {
			maxPlayer = int.Parse (maxPlayerString);

			// Input check for max player.
			if (maxPlayer > GameManager.Instance.NetManager.maxPlayer)
				maxPlayer = GameManager.Instance.NetManager.maxPlayer;
			if (maxPlayer == 0)
				maxPlayer = 1;
		} else {
			maxPlayer = 1;
		}

		GUILayout.BeginVertical ();
		GUILayout.Label ("", GUILayout.Height (labelFontSize+gapSize), GUILayout.Width (labelWidth-gapSize));
		if (GUILayout.Button ("Create Room ", btnStyle, 
			GUILayout.Height (inputFontSize+gapSize*7), GUILayout.Width (labelWidth-gapSize*10))) {
			if (roomName != "" && maxPlayer > 0) {
				
				GameManager.Instance.ScManager.StartMultiplayScene ();
				GameManager.Instance.NetManager.NewRoom (roomName, maxPlayer);
				GUIManager.CloseLobby ();
			}
		}
		GUILayout.EndVertical ();
		GUILayout.EndHorizontal ();

		GUILayout.Space (gapSize);
		GUI.color = Color.yellow;
		GUILayout.Box ("Sessions Open:", labelStyle, 
			GUILayout.Height (btnHeight), GUILayout.Width (labelWidth*2));

		scrollPositionForLobby = 
			GUILayout.BeginScrollView (scrollPositionForLobby, false, false, 
				GUILayout.Width (labelWidth*2), GUILayout.Height (btnWidth*2-75));

		GUI.color = Color.green;
		// Displays available rooms.
		foreach (RoomInfo room in PhotonNetwork.GetRoomList ()) {
			// Avoids displaying the room with no-one inside.
			if (room.playerCount == 0)
				continue;

			GUILayout.BeginHorizontal ();
			GUILayout.Box (room.name + " : " + room.playerCount + "/" + room.maxPlayers, sessionStyle,
				GUILayout.Height (boxFontSize+gapSize), GUILayout.Width (labelWidth + btnHeight + gapSize*5));

			GUILayout.Space (gapSize*5);

			if (GUILayout.Button ("Join", btnStyle, 
				GUILayout.Height (boxFontSize+gapSize), GUILayout.Width (btnHeight))) {
				
				GameManager.Instance.ScManager.StartMultiplayScene ();
				GameManager.Instance.NetManager.JoinSession (room.name);
				GUIManager.CloseLobby ();
			}
			GUILayout.EndHorizontal ();
		}

		GUILayout.EndScrollView ();
		GUIManager.InsertEndSpace ();
		GUILayout.EndArea ();
	}
}
