﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class provides the login UI and the login functionality
/// which allows the user to login into their account with
/// the user name and the password.
/// </summary>
public class Login : MonoBehaviour {

	// Used for GUI flow control in the login screen.
	private static bool isInputEmpty = false;
	private static bool hasLoginFailed = false;
	private static bool hasLoginSkipped = false;
	private static bool loginAttempting = false;

	private static int loginBtnFontSize = 28; // Font size for login button text.
	private static int loginLabelFontSize = 28; // Font size for login label text.
	private static int loginWarnFontSize = 24; // Font size for warning message in login screen.
	private static int loginInputFontSize = 24; // Font size for warning message in login screen.
	private static int loginLabelWidth = Screen.width/5;	// Width of login label.
	private static int loginTextFieldWidth = Screen.width/4; // Width of login text.
	private static int loginButtonWidth = Screen.width/5; // Width of login button.
	private static int loginTestFieldHeight = Screen.height/15; // Height of login text field.
	private static int loginButtonHeight = Screen.height/10; // Height of login button.

	// Used as params for login GUI functions.
	static string playerName = "";
	static string defaultPlayerName = "player";
	static string password = "";
	static string defaultPassword = "pass";

	/// <summary>
	/// Helper methods to set parameters when login screen is terminating.
	/// </summary>
	/// <param name="skipped"> If set to <c>true</c> it indicates that the login has been skipped (not complete).</param>
	private static void AfterLoginScreen (bool skipped)
	{
		GameManager.Instance.IsLoggedIn = !skipped;
		GameManager.Instance.SetPlayerName (playerName);
		loginAttempting = false;
		GUIManager.CloseLoginScreen ();
	}

	void OnGUI ()
	{
		if (!GUIManager.IsLoginGUIEnabled ())
			return;

		GUI.depth = 1;

		// Style for login button.
		GUIStyle btnStyle = new GUIStyle (GUI.skin.button);
		btnStyle.fontSize = loginBtnFontSize;
		btnStyle.normal.textColor = Color.white;

		// Style for login label.
		GUIStyle labelStyle = new GUIStyle (GUI.skin.label);
		labelStyle.fontSize = loginLabelFontSize;
		labelStyle.normal.textColor = Color.white;

		// Style for input text.
		GUIStyle inputStyle = new GUIStyle (GUI.skin.textField);
		inputStyle.normal.textColor = Color.grey;
		inputStyle.fontStyle = FontStyle.Italic;
		inputStyle.fontSize = loginInputFontSize;

		// Style for warning text.
		GUIStyle warningStyle = new GUIStyle (GUI.skin.label);
		warningStyle.normal.textColor = Color.red;
		warningStyle.fontSize = loginWarnFontSize;


		// This avoid the need to press login button multiple times.
		if (loginAttempting) {
			if (GameManager.Login (playerName, password)) {
				AfterLoginScreen (false);
				return;
			} else
				loginAttempting = false;
		}

		GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
		GUI.color = Color.white;
		GUILayout.BeginArea (new Rect (0, 0, Screen.width, Screen.height));

		GUIManager.InsertStartSpace ();

		// Room name input textbox.
		GUILayout.BeginHorizontal ();
		GUI.color = Color.white;
		GUILayout.Label ("Player Name:", labelStyle, GUILayout.Width (loginLabelWidth));
		playerName = defaultPlayerName = GUILayout.TextField (defaultPlayerName, inputStyle,
			GUILayout.Height (loginTestFieldHeight), GUILayout.Width (loginTextFieldWidth)).Trim ();
		GUILayout.EndHorizontal ();

		// Room name input textbox.
		GUILayout.BeginHorizontal ();
		GUI.color = Color.white;
		GUILayout.Label ("Password:   ", labelStyle, GUILayout.Width (loginLabelWidth));
		password = defaultPassword = GUILayout.TextField (defaultPassword, inputStyle,
			GUILayout.Height (loginTestFieldHeight), GUILayout.Width (loginTextFieldWidth)).Trim ();
		GUILayout.EndHorizontal ();

		GUILayout.Label ("", GUILayout.Width (loginLabelWidth));
		GUILayout.BeginHorizontal ();

		// Login button display & event handler.
		if (GUILayout.Button ("login", btnStyle, GUILayout.Height (loginButtonHeight), 
			    GUILayout.Width (loginButtonWidth))) {
			isInputEmpty = false;
			hasLoginFailed = false;

			if (playerName == "" || password == "") 
				// Invalid input.
				isInputEmpty = true;
			else {
				loginAttempting = true;
				if (GameManager.Login (playerName, password))
					// Login success.
					AfterLoginScreen (false);
				else {
					// Login failure.
					hasLoginFailed = true;
					// Login failure means input was valid.
					isInputEmpty = false;
				}
			}
		}

		GUILayout.Space (loginButtonWidth / 5);
		if (GUILayout.Button ("skip login", btnStyle, GUILayout.Height (loginButtonHeight), 
			    GUILayout.Width (loginButtonWidth))) {
			// Login has been skipped. No Database access nor multiplayer mode available.
			AfterLoginScreen (true);
		}

		GUILayout.EndHorizontal ();

		/// Displays error/warning messages.
		if (hasLoginFailed)
			GUILayout.Label ("Login failed", warningStyle, GUILayout.Width (loginLabelWidth * 2));
		else if (isInputEmpty)
			GUILayout.Label ("ERROR: input is empty", warningStyle, GUILayout.Width (loginLabelWidth * 2));
		else
			GUILayout.Label ("", GUILayout.Width (loginLabelWidth));

		GUIManager.InsertEndSpace ();

		GUILayout.EndArea ();
	}
}
