﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//This class represents the Network Player. It mainly deals
//with how network interactions are handled, including things
//like if a player moves, this information is sent to all other
//players so they can keep in sync with other players movements.
//Futhermore this script also handles the syncing of skill animations,
//and potion animations.

public class NetworkPlayer : Photon.MonoBehaviour, INetworkPlayer {

	//Animation animationObject;
	private bool isRunning;
	private bool isDown;

	// These are used to lerp synchronise player's rotation and velocity.
	private Rigidbody rbody;
	private Quaternion playerRotation;
	private Vector3 playerPosition;
	// Used to adjust position & rotation sync timing.
	private int syncAdjust = 5;
		
	public bool IsDown
	{
		get {
			return isDown;
		}
		set {
			isDown = value;
		}
	}

	
	// Use this for initialization
	void Awake () {

		//The master client is responsible for the enemy bots spawning
		//from there on it informs other connected players on the bots
		//locations etc.
		if (PhotonNetwork.isMasterClient) {

			GameObject enemyManager = GameObject.Find ("EnemyManager");
			enemyManager.GetComponent<EnemyManager> ().enabled = true;
		}

		if (photonView.isMine) {

			// Setup for myself
			Init ();

		} else {

			// NPC setup
			Player npc = GetComponent <Player> ();
			GetComponent <JoystickController> ().enabled = false;
			npc.netPlayer = this;
			npc.anim = npc.GetComponent <Animation> ();
			npc.healthSlider = GameObject.Find ("Slider").GetComponent <Slider> ();
			npc.damageImage = GameObject.Find ("DamageImage").GetComponent <Image> ();

		}

		// Rotation and velocity setup.
		rbody = GetComponent <Player> ().GetComponent <Rigidbody> ();
		playerRotation = transform.rotation;
		playerPosition = rbody.position;
	}

	// Initialises the game for multiplayer mode.
	private void Init ()
	{
		// Initialises the player.
		Player player = GetComponent <Player> ();
		player.netPlayer = this;
		player.weapon = player.GetComponent <Weapon> ();
		player.anim = player.GetComponent <Animation> ();
		// Initialises the scene.
		GameManager.Instance.ScManager.InitScene (player);
	}
		
	/// <summary>
	/// Returns PhotonView object.
	/// </summary>
	/// <returns>The photon view.</returns>
	public PhotonView GetPhotonView ()
	{
		return photonView;
	}

	// Update is called once per frame
	void Update () {

		//We only want to animate if it is not our player
		if (!photonView.isMine) {

			// Lerping player's rotation and position.
			transform.rotation = Quaternion.Lerp (transform.rotation, playerRotation, Time.deltaTime * syncAdjust);
			transform.position = Vector3.Lerp (transform.position, playerPosition, Time.deltaTime * syncAdjust);

			if (isRunning) {
				//GetComponent<Animation> ().Play ("Walk");
				PlayAnim ("Walk");
			} else {
				//GetComponent<Animation> ().Play ("Wait");
				PlayAnim ("Wait");
				rbody.velocity = Vector3.zero;
			}
		} else
			rbody.velocity = Vector3.zero;
				
	}

	//Plays the animation depending on what animations are not playing
	private void PlayAnim (string animToPlay)
	{
		if (isDown)
			return;
		
		if (!AnimationIsPlaying ("Attack") && !AnimationIsPlaying ("Damage") && !AnimationIsPlaying ("Dead"))
			GetComponent <Animation> ().Play (animToPlay);
	}

	//This method is responsible for seralising data, and sending in byte form
	//to all other connected clients. It will also check the stream for incoming 
	//messages from other clients.
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {

		if (stream.isWriting) {

			//This is out player, so we need to send other players our data
			stream.SendNext(AnimationIsPlaying("Walk"));
			stream.SendNext (transform.rotation); 
			stream.SendNext (transform.position);

		} else {

			// Network player, we need to receive the data from other players
			isRunning = (bool)stream.ReceiveNext();
			playerRotation = (Quaternion) stream.ReceiveNext();
			playerPosition = (Vector3)stream.ReceiveNext ();
		}
	}

	//Check to see if a certain animation is currently playing
	bool AnimationIsPlaying(string stateName) {

		return GetComponent<Animation>().IsPlaying(stateName);
	}

	// RPC calls
	public void PlayAnimRPC (string animToPlay)
	{
		PhotonView.Get (this).RPC ("PlayAnim", PhotonTargets.All, animToPlay);
	}

	public void SkillPRC ()
	{
		PhotonView.Get (this).RPC ("UseSkill", PhotonTargets.All);
	}

	public void PotionRPC ()
	{
		PhotonView.Get (this).RPC ("UsingPotion", PhotonTargets.All);
	}	
		
	public void PlayAttackSoundRPC ()
	{
		PhotonView.Get (this).RPC ("playAttackSound", PhotonTargets.All);
	}

	public void PlayHitSoundRPC ()
	{
		PhotonView.Get(this).RPC ("playHitSound", PhotonTargets.All );
	}

	public void TakeDamageRPC (int photonId, float damage)
	{
		PhotonView.Get(this).RPC ("TakeDamage", PhotonTargets.All, photonId, damage);
	}
	
	public void TakeDownRPC ()
	{
		PhotonView.Get(this).RPC ("TakeDown", PhotonTargets.All);
	}

	/// <summary>
	/// Sends the message to display the revive button on the downed player
	/// on other people's screens.
	/// </summary>
	/// <param name="photonId">Photon identifier.</param>
	public void DisplayReviveRPC(int photonId)
	{
		PhotonView.Get (this).RPC ("DisplayRevive", PhotonTargets.All,photonId);
	}

	/// <summary>
	/// Resets the reviving status of the person being revived and sends
	/// this updated information to everother client.
	/// </summary>
	/// <param name="photonId">Photon identifier.</param>
	public void ResetReviveRPC(int photonId) {

		PhotonView.Get (this).RPC ("ResetRevive", PhotonTargets.All, photonId);
	}

	/// <summary>
	/// This message is sent when a player gets revived, we need to tell
	/// everyother client that the player has been revived and tell them
	/// to update their screens.
	/// </summary>
	/// <param name="photonId">Photon identifier.</param>
	public void RevivedRPC(int photonId)
	{

		PhotonView.Get (this).RPC ("Revived", GetTarget(photonId));
	}

	/// <summary>
	/// This updates the reviving flag, to prevent two people revivng the same person
	/// essentially.
	/// </summary>
	/// <param name="photonId">Photon identifier.</param>
	/// <param name="gettingRevived">If set to <c>true</c> getting revived.</param>
	public void GettingRevivedRPC(int photonId,bool gettingRevived)
	{
		PhotonView.Get (this).RPC ("GettingRevived", PhotonTargets.Others, photonId,gettingRevived);
	}


	// Find a PhotonPlayer given photonId
	private PhotonPlayer GetTarget (int photonId)
	{
		return PhotonPlayer.Find (photonId);
	}
}
