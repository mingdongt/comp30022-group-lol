﻿using UnityEngine;
using System.Collections;

public class NetworkEnemy : Photon.MonoBehaviour, INetworkPlayer
{
	private bool isRunning;
	private bool isDown;

	// These are used to lerp synchronise player's rotation and velocity.
	private Rigidbody rbody;
	private Quaternion playerRotation;
	private Vector3 playerPosition;
	// Used to adjust position & rotation sync timing.
	private int syncAdjust = 5;

	void Awake ()
	{
		Init ();
		// Turn off some features.
		if (!PhotonNetwork.isMasterClient) {
			Enemy npc = GetComponent <Enemy> ();
			npc.GetComponent <Enemy> ().enabled = false;
			npc.GetComponent <NavMeshAgent> ().enabled = false;
		} 
	}

	/// <summary>
	/// Used to check if the character is down.
	/// </summary>
	/// <value><c>true</c> if this character is down; otherwise, <c>false</c>.</value>
	public bool IsDown
	{
		get {return isDown;}
		set {isDown = value;}
	}

	// Initialises the game for multiplayer mode.
	private void Init ()
	{
		// Initialises the player.
		Enemy enemy = GetComponent <Enemy> ();
		enemy.netPlayer = this;
		enemy.weapon = enemy.GetComponent <Weapon> ();
		enemy.anim = enemy.GetComponent <Animation> ();
		// Rotation and velocity setup.
		rbody = GetComponent <Enemy> ().GetComponent <Rigidbody> ();
		playerRotation = enemy.transform.rotation;
		playerPosition = enemy.transform.position;
	}

	void Update () 
	{
		//We only want to animate if it is not our player
		if (!PhotonNetwork.isMasterClient) 
		{
			// Lerping player's rotation and position.
			transform.rotation = Quaternion.Lerp (transform.rotation, playerRotation, Time.deltaTime * syncAdjust);
			transform.position = Vector3.Lerp (transform.position, playerPosition, Time.deltaTime * syncAdjust);
			if (rbody != null)
				rbody.velocity = Vector3.zero;
			
			// Update animation.
			if (isRunning) 
				PlayAnim ("Walk");
			else
				PlayAnim ("Wait");
		}
	}
		
	/// <summary>
	/// Plays the animation depending on what animations are not playing
	/// </summary>
	/// <param name="animToPlay">Animation to play.</param>
	private void PlayAnim (string animToPlay)
	{
		if (isDown)
			return;

		if (!AnimationIsPlaying ("Attack") && !AnimationIsPlaying ("Damage") && !AnimationIsPlaying ("Dead"))
			GetComponent <Animation> ().Play (animToPlay);
	}

	/// <summary>
	/// Returns PhotonView object.
	/// </summary>
	/// <returns>The photon view.</returns>
	public PhotonView GetPhotonView ()
	{
		return photonView;
	}
		
	/// <summary>
	/// This method is responsible for seralising data, and sending in byte form
	/// to all other connected clients. It will also check the stream for incoming 
	/// messages from other clients.
	/// </summary>
	/// <param name="stream">PhotonStream object used to send the data.</param>
	/// <param name="info">PhotonMessageInfo containing info about the sender.</param>
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) 
	{

		// Sends enemy info to other players' game instances.
		if (PhotonNetwork.isMasterClient && stream.isWriting) 
		{
			//This is out player, so we need to send other players our data
			stream.SendNext (AnimationIsPlaying ("Walk"));
			stream.SendNext (transform.rotation); 
			stream.SendNext (transform.position);
		} 

		// Receive enemy info from the master client.
		if (!PhotonNetwork.isMasterClient && !stream.isWriting)  
		{
			// Network player, we need to receive the data from other players
			isRunning = (bool)stream.ReceiveNext ();
			playerRotation = (Quaternion)stream.ReceiveNext ();
			playerPosition = (Vector3)stream.ReceiveNext ();
		}
	}

	//Check to see if a certain animation is currently playing
	bool AnimationIsPlaying(string stateName) 
	{
		return GetComponent<Animation>().IsPlaying(stateName);
	}

	/// <summary>
	/// RPC call for playing animation.
	/// </summary>
	/// <param name="animToPlay">Animation to play.</param>
	public void PlayAnimRPC (string animToPlay)
	{
		PhotonView.Get (this).RPC ("PlayAnim", PhotonTargets.All, animToPlay);
	}
		
	public void PlayAttackSoundRPC ()
	{
		PhotonView.Get (this).RPC ("playAttackSound", PhotonTargets.All);
	}

	public void PlayHitSoundRPC ()
	{
		PhotonView.Get(this).RPC ("playHitSound", PhotonTargets.All );
	}

	public void TakeDamageRPC (int photonId, float damage)
	{
		PhotonView.Get(this).RPC ("TakeDamage", PhotonTargets.All, photonId, damage);
	}

	public void TakeDownRPC ()
	{
		PhotonView.Get(this).RPC ("TakeDown", PhotonTargets.All);
	}

	public void SkillPRC ()
	{
		Debug.LogError ("This function should not be called for enemy");
	}

	public void PotionRPC ()
	{
		Debug.LogError ("This function should not be called for enemy");
	}

	public void DisplayReviveRPC (int id)
	{
		Debug.LogError ("This function should not be called for enemy");
	}	
		
	/// <summary>
	/// Finds a PhotonPlayer given photonId
	/// </summary>
	/// <returns>The target.</returns>
	/// <param name="photonId">Photon identifier.</param>
	private PhotonPlayer GetTarget (int photonId)
	{
		return PhotonPlayer.Find (photonId);
	}
}
