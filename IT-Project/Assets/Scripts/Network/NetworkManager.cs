﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The network manager is responsible for creating and managing
/// multiplayer sessions, It helps clients connect and join lobbies
/// and also instantiates their playe into the world.
/// </summary>
public class NetworkManager : MonoBehaviour 
{
	private static NetworkManager instance;
	private const string VERSION = "v1.0.0";
	public int maxPlayer = 5;
	public Transform spawnPoint;
	private bool connected;

	void Awake ()
	{
		if (instance == null) 
		{
			instance = this;
		} else {
			Destroy (gameObject);
		}
			
		this.enabled = false;
	}

	public bool Connected 
	{
		get 
		{
			return connected;
		}
	}
		
	/// <summary>
	/// Start this instance. Use this for initialization
	/// </summary>
	void Start () 
	{
			Connect();
	}

	void OnJoinedLobby() 
	{
		Debug.Log (PhotonNetwork.playerName + " has joined in the lobby.");
	}

	void OnCreatedRoom ()
	{
		if (PhotonNetwork.room != null)
			Debug.Log ("Room " +PhotonNetwork.room.name + " has been created.");
	}

	void OnJoinedRoom() 
	{
		if (connected)
			InstantiatePlayer ();
	}
		
	void Connect()
	{
		if (!PhotonNetwork.connected)
			connected = PhotonNetwork.ConnectUsingSettings (VERSION);
	}

	public void Reconnect ()
	{
		PhotonNetwork.Disconnect ();
		connected = PhotonNetwork.ConnectUsingSettings (VERSION);
	}
		
	/// <summary>
	/// Instantiate the specified prefab of a player.
	/// </summary>
	public void InstantiatePlayer ()
	{
		PhotonNetwork.Instantiate (GameManager.Instance.multiPlayerPrefabName, spawnPoint.position, spawnPoint.rotation, 0);
	}
		
	/// <summary>
	/// Creates a new room and join in it. If the room already exists, just join in it.
	/// </summary>
	/// <param name="roomName">Room name to join in or create.</param>
	/// <param name="maxPlayer">Max number of player in the room (when a new room is created).</param>
	public void NewRoom (string roomName, int maxPlayer)
	{
		if (roomName != "" && maxPlayer > 0) 
		{
			PhotonNetwork.JoinOrCreateRoom (roomName, new RoomOptions () { MaxPlayers = (byte) maxPlayer }, null);
		}
	}
		
	/// <summary>
	///Joins in the game session that already exists.
	/// </summary>
	/// <param name="roomname">Room name to join in.</param>
	public void JoinSession (string roomname)
	{
		PhotonNetwork.JoinRoom (roomname);
	}

	/// <summary>
	/// Displays Lobby GUI.
	/// </summary>
	void OnGUI()
	{
		if (!PhotonNetwork.insideLobby)
			return;

		GUIManager.ShowLobby ();
	}

	/// <summary>
	/// Set up player name.
	/// </summary>
	/// <param name="name">The player name to be set to the current game instance.</param>
	public void SetPlayerName (string name)
	{
		PhotonNetwork.playerName = name;
	}
		
	/// <summary>
	/// Returns the name of the player.
	/// </summary>
	/// <returns>The player name.</returns>
	public string GetPlayerName ()
	{
		return PhotonNetwork.playerName;
	}
}
