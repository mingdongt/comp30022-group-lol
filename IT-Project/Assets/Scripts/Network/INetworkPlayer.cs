﻿using UnityEngine;
using System.Collections;

public interface INetworkPlayer 
{
	bool IsDown { get; set; }
	void PlayAnimRPC (string animToPlay);
	void PlayAttackSoundRPC ();
	void PlayHitSoundRPC ();
	void TakeDamageRPC (int photonId, float damage);
	void TakeDownRPC ();
	void SkillPRC ();
	void PotionRPC ();
	void DisplayReviveRPC (int id);
	PhotonView GetPhotonView ();
}
