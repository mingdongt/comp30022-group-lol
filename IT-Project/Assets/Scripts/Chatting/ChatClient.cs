﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System;
using System.IO;
using System.Text;
using System.Threading;

public class ChatClient : MonoBehaviour {

	TcpClient tcpclient;
	Stream stm;
	ASCIIEncoding asen;
	public List<string> chatHistory;
	private string currentMessage = string.Empty;
	private System.Object lockThis = new System.Object ();
	Thread t;

	// Use this for initialization
	void Start () {


		ConnectChatClient ();

	}
	
	// Update is called once per frame
	void Update () {


	}

	void OnGUI()
	{


		
		GUILayout.BeginHorizontal (GUILayout.Width (250));
		currentMessage = GUILayout.TextField (currentMessage);


		if (GUILayout.Button ("Send")) {

			if (!string.IsNullOrEmpty (currentMessage.Trim ())) {
				byte[] byteMessage = asen.GetBytes (currentMessage);
				stm.Write (byteMessage, 0, byteMessage.Length);
				chatHistory.Add (currentMessage);
				currentMessage = string.Empty;
			}
		}

		GUILayout.EndHorizontal ();

		foreach (string c in chatHistory) {
			GUILayout.Label (c);
		}
	}


	private void readMessages() {

	
		while (true) {	
			byte[] incomingMessage = new byte[256];
			int length = stm.Read (incomingMessage, 0, 256);



			string message = System.Text.Encoding.Default.GetString (incomingMessage);
			chatHistory.Add (message);
		}

	}

	public bool ConnectChatClient() {

		bool connected = false;

		try {
			tcpclient = new TcpClient ();
			tcpclient.Connect ("115.146.89.204", 8080);
			chatHistory = new List<string>();
			Debug.Log("Connecting.....");
			//String name = "test";
			stm = tcpclient.GetStream();
			asen = new ASCIIEncoding();

			t = new Thread (readMessages);
			t.Start ();
			connected = true;

		}
		catch (Exception e) {
			Console.WriteLine ("Error......");
		}

		return connected;
	}


}
