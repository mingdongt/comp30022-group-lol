import sys
import socket
import time
import thread
import select

HOST = '121.221.71.199'
SOCKET_LIST = []
RECV_BUFFER = 1024
PORT = 8080
MAX_CONNECTIONS = 4

 

def sendAll(server_socket,sock,message):
    print message
    for socket in SOCKET_LIST:

        if socket != server_socket and socket != sock:

            try:
                socket.send(message)
            except:
                socket.close()
                if socket in SOCKET_LIST:
                    SOCKET_LIST.remove(socket)


def chat_server():

    server_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
    #server_socket.setblocking(False)
    server_socket.bind((HOST,PORT))
    server_socket.listen(MAX_CONNECTIONS)

    # readable connections
    SOCKET_LIST.append(server_socket)

    print "Chat server started on port "+ str(PORT)


    while True:

        ready_to_read, ready_to_write, in_error = select.select(SOCKET_LIST,[],[],0)

        for sock in ready_to_read:

            # we got a new connection
            if sock == server_socket:

                sockfd, addr = server_socket.accept()
                SOCKET_LIST.append(sockfd)
                print "Client Connected"
                sendAll(server_socket,sockfd,"Client entered our chatting room\n")

            else:

                try:
                    message = sock.recv(RECV_BUFFER)

                    if message:
                        sendAll(server_socket,sock,message+"\n")

                    else:

                        if sock in SOCKET_LIST:
                            SOCKET_LIST.remove(sock)

                        #no data from this connection, so maybe have it send it disconnected to everyone?

                except:
                    
                    sendAll(server_socket,sock,"Client went offline\n")
                    continue

    server_socket.close()


if __name__ == "__main__":

    sys.exit(chat_server()) 
