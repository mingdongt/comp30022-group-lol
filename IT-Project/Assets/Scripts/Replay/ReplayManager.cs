﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Manages what replay gets run, keeping track of the replay
/// name.
/// </summary>
public class ReplayManager : MonoBehaviour {

	public string replayName;


	void Awake() {

		DontDestroyOnLoad(transform.gameObject);
	}

	// Use this for initialization
	void Start () {

		replayName = string.Empty;
	}

	public void StartReplay(string name) {

		replayName = name;
		Application.LoadLevel ("ReplayMovie");
	}
}
