﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


/// <summary>
/// This holds the logic realted to the record
/// button.
/// </summary>
public class RecordButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	Capture capture;

	// Use this for initialization
	void Start () {
	
		capture = GameObject.Find ("Recorder").GetComponent<Capture> ();

		if (capture == null) {
			Debug.Log ("The Recorder Object is null. Please Fix This Issue.");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public virtual void OnPointerDown (PointerEventData ped)
	{


		capture.Record ();

	}

	public virtual void OnPointerUp (PointerEventData ped)
	{
		// Currently nothing is done here.
	}
}
