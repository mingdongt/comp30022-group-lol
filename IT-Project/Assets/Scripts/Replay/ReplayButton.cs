﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;


/// <summary>
/// This class is responsible for defining
/// functionality of the replay button
/// </summary>
public class ReplayButton : MonoBehaviour {



	int timer;

	void Start() {

		timer = 0;
	}

	void Update() {

		timer++;

	}


	/// <summary>
	/// Plays the replay.
	/// </summary>
	public void PlayReplay() {

		string name = this.GetComponentInChildren<Text> ().name;

		if (name != null)
			GameObject.Find ("ReplayManager").GetComponent<ReplayManager> ().StartReplay (name);
		else
			Debug.Log ("Directory name for the replay was null.");

	}

	public void buttonPressed() {

		timer = 0;

	}

	/// <summary>
	/// When user lets go of the button, if the button was held for approx
	/// 1 second then the replay gets deleted.
	/// </summary>
	public void StoppedPressingButton() {

		if (timer >= 40) {

			string name = this.GetComponentInChildren<Text> ().name;
			deleteContentsFromDirectory (name);
			Destroy (this.gameObject);

		}

	}


	/// <summary>
	/// Deletes the contents from directory and the directory.
	/// </summary>
	/// <returns>The contents from directory.</returns>
	/// <param name="name">Name.</param>
	public void deleteContentsFromDirectory(string name) {
	
		DirectoryInfo dir = new DirectoryInfo (Application.persistentDataPath + "/" + name);

		FileInfo[] files = dir.GetFiles ();

		foreach (FileInfo file in files) {
			file.Delete ();
		}

		dir.Delete ();

	}
}
