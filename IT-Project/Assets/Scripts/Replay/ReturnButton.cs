﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


/// <summary>
/// This button returns the user back to the main menu.
/// </summary>
public class ReturnButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler  {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public virtual void OnPointerDown (PointerEventData ped)
	{
		Destroy(GameObject.Find ("ReplayManager"));
		Application.LoadLevel ("sc_Title");

	}

	public virtual void OnPointerUp (PointerEventData ped)
	{
		// Currently nothing is done here.
	}
}
