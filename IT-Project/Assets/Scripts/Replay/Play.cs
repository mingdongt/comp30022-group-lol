﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;
using System.IO;
using System.Linq;
using System;

/// <summary>
/// This class is responsible for playing the
/// sequence of images. It does through first loading the
/// sequence of images, and then runs through each image at
/// rate of FPS defined.
/// </summary>
public class Play : MonoBehaviour {

	/// <summary>
	/// Frames per second for the sequence.
	/// </summary>
	public float FPS;

	/// <summary>
	/// The max frames used in the recording.
	/// </summary>
	private const int MAX_FRAMES = 100;

	/// <summary>
	/// The current texture that is being displayed.
	/// </summary>
	Texture2D current;

	/// <summary>
	/// The textures for the loaded sequence.
	/// </summary>
	List<Texture2D> textures;


	bool isPlaying;
	int index;
	bool playMovie;

	public bool IsPlaying {

		get {
			return isPlaying;
		}
	}

	public Texture2D Current {

		get {
			return current;
		}
	}

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start() {

		playMovie = false;
		textures = new List<Texture2D> ();
		loadSequence ();
		PlayMovie ();
	}


	float time = 0;

	void Update() {


		if (isPlaying) {

			time += Time.deltaTime;

			if (time > 1f / FPS) {
				time = 0;
				PlayBack ();
			}
		}

	}

	/// <summary>
	/// Plaies the movie.
	/// </summary>
	public void PlayMovie() {

		if (!isPlaying) {

			index = 0;
			isPlaying = true;
			time = (1f / FPS);
			playMovie = true;
		}

	}

	/// <summary>
	///  PlayBack the movie.
	/// </summary>
	void PlayBack() {

		if (isPlaying) {

			if (textures != null) {

				if (textures.Count > 0) {

					current = textures.ElementAt (index);
					index++;

					if (index == textures.Count) {

						index = 0;
						isPlaying = false;
						playMovie = false;
						Destroy (GameObject.Find ("ReplayManager"));
						Application.LoadLevel ("ReplayList");
					}
				}


			}
		}
		
	}


	void OnGUI() {

		if (playMovie) {

			if (current != null) {

				GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), current);
			}
		}
	}


	/// <summary>
	/// Loads the sequence.
	/// </summary>
	void loadSequence() {

		string name = GameObject.Find ("ReplayManager").GetComponent<ReplayManager> ().replayName;
		string path = Application.persistentDataPath + "/" + name;



		for(int i=0;i<MAX_FRAMES;i++) {

			string fileName = "screen" + i +".jpg";
			byte[] bytes;
			Texture2D tex;
			bytes = System.IO.File.ReadAllBytes (path + "/" + fileName);
			tex = new Texture2D (Screen.width, Screen.height);
			tex.LoadImage (bytes);
			textures.Add (tex);

		}



	}



}
