﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.IO;
using UnityEngine.UI;



/// <summary>
/// This class is responsible for taking the actual screenshots, saving the
/// screenshots. This class will create folder in persistent data folder on
/// the android phone.
/// </summary>
public class Capture : MonoBehaviour {


	/// <summary>
	/// The max frames to be stored
	/// </summary>
	public int maxFrames;

	List<Texture2D> screenShots;
	PauseButton pauseButton;

	/// <summary>
	/// Flag to identify if the current replay is saved.
	/// </summary>
	bool saved;

	/// <summary>
	/// counter to determine interval for screen capture.
	/// </summary>
	int counter;

	/// <summary>
	/// flag to determine if we are in record mode or not.
	/// </summary>
	bool record;


	GameObject recordButton;

	//The notifications used, to give the user
	//idication of the state of capture.
	Text recording;
	Text pauseToSaveReplay;
	Text replaySaved;


	public bool RecordStatus {

		get {
			return record;
		}
		set {
			record = value;
		}
	}

	public List<Texture2D> ScreenShots {

		get {
			return ScreenShots;
		}
	}

	// Use this for initialization
	void Start () {

		saved = true;
		counter = 0;
		record = false;
		screenShots = new List<Texture2D>();
		pauseButton = GameObject.Find ("pauseButtonHost").GetComponent<PauseButton> ();

		//notifications
		recording = GameObject.Find ("Recording").GetComponent<Text> ();
		pauseToSaveReplay = GameObject.Find ("PauseToSaveReplay").GetComponent<Text> ();
		replaySaved = GameObject.Find ("ReplaySaved").GetComponent<Text> ();


		recordButton = GameObject.Find ("Record");
	}
	
	// Update is called once per frame
	void Update () {

		if (!pauseButton.paused && !pauseButton.pressed) {

			replaySaved.enabled = false;

			if (record) {

				recording.enabled = true;
				pauseToSaveReplay.enabled = false;

				// only a ceratin amount of screenshots per second or per ~30-40 frames
				if (counter < 10) {
					StopCoroutine (SaveScreenShots ());
				} else if (counter == 10) {
					counter = 0;
					StartCoroutine (SaveScreenShots ());

				}
					
				saved = false;
				counter++;
			} else {

				recording.enabled = false;

				if (screenShots.Count > 0) {
					pauseToSaveReplay.enabled = true;
				}
			}
		}
		else {

			recording.enabled = false;
			pauseToSaveReplay.enabled = false;

			if (!saved) {
				writeScreenshots ();		
			}

			if (saved) {
				replaySaved.enabled = true;
			}
		}
	
			
	}

	/// <summary>
	/// Saves the screen shots.
	/// </summary>
	/// <returns>The screen shots.</returns>
	private IEnumerator SaveScreenShots() {

		//Wait for graphics to render
		yield return new WaitForEndOfFrame();

		RenderTexture rt = new RenderTexture(Screen.width/2, Screen.height/2, 24);        
		Texture2D screenShot = new Texture2D(Screen.width/2, Screen.height/2, TextureFormat.RGB24, false);

		foreach(Camera cam in Camera.allCameras)
		{
			
			cam.targetTexture = rt;
			cam.Render();
			cam.targetTexture = null;
		}

		RenderTexture.active = rt;
		screenShot.ReadPixels(new Rect(0,0, Screen.width, Screen.height), 0, 0);
		Camera.main.targetTexture = null;
		RenderTexture.active = null; //Added to avoid errors
		Destroy(rt);

		//Split the process up
		yield return 0;

		if (screenShots.Count == maxFrames) {
			record = false;
			Debug.Log ("Got 10 Seconds worth of footage");
		} else {
			screenShots.Add (screenShot);
		}
	}


	/// <summary>
	/// Writes the screenshots to a newly created directory.
	/// </summary>
	private void writeScreenshots() {

		int index = 0;
		string folderName = "Screenshots" + index;
		DirectoryInfo dirInfo = new DirectoryInfo (Application.persistentDataPath + "/" + 
			folderName);

	
		while (dirInfo.Exists) {

			index++;
			folderName = "Screenshots" + index;
			dirInfo = new DirectoryInfo (Application.persistentDataPath + "/" + 
				folderName);
		}


		dirInfo.Create ();
		Debug.Log ("Creating Sub-Directory");

		for (int i = 0; i < screenShots.Count; i++) {

			Texture2D tex = screenShots.ElementAt (i);
			byte[] shot = tex.EncodeToJPG ();
			File.WriteAllBytes (Application.persistentDataPath + "/"+folderName + "/screen" + i + ".jpg",shot);
		}
		screenShots.Clear ();
		saved = true;
	}


	/// <summary>
	/// Record this instance.
	/// </summary>
	public void Record() {

		if (!record) {


			Debug.Log ("Recording");
			record = true;
			screenShots.Clear ();
			counter = 0;

		} 

	}
}
