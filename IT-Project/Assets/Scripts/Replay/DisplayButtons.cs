﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;


/// <summary>
/// This class is responsible for displaying the buttons
/// on the replay list, it does this through looking at
/// the folders available in the persistent data folder
/// on the android.
/// </summary>
public class DisplayButtons : MonoBehaviour {

	public GameObject ListItemMovie;
	public GameObject ContentPanel;

	// Use this for initialization
	void Start () {

		InitialiseAllButtons ();

	}
	
	// Update is called once per frame
	void Update () {


	}


	/// <summary>
	/// Initialises all buttons, By going through
	/// all the available directories.
	/// </summary>
	void InitialiseAllButtons() {

		DirectoryInfo dir = new DirectoryInfo (Application.persistentDataPath);

		DirectoryInfo[] directories = dir.GetDirectories ();
		int numDir = directories.Length;


		for (int i = 0; i < numDir; i++) {
			
			GameObject newMovie = Instantiate (ListItemMovie) as GameObject;
			newMovie.GetComponentInChildren<Text> ().name = "Screenshots" + i;
			newMovie.GetComponentInChildren<Text> ().text = "Replay "+i;
			newMovie.transform.parent = ContentPanel.transform;
		}

	}


}
