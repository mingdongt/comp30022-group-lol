﻿using UnityEngine;
using System.Collections;


//This test is used to confirm whether the player spwans with the correct
//attributes, it also ensures that there is no inconsistency between what
//attributes we expect the player to have and what attributes the player
//actually has. This Test will fail if one of the attributes does not match
//with the expected attributes
public class Player_Init_Test : MonoBehaviour {

	// Use this for initialization
	private const float EXPECTED_SWORD_RANGE = 100f;
	private const int EXPECTED_SWORD_DAMAGE = 50;
	private const int EXPECTED_SWORD_ATTACK_RADIUS = 20;
	private const int EXPECTED_HEALTH_POINT = 100;
	private const float EXPECTED_RESISTANCE = 0.5f;
	private const float EXPECTED_STRENGTH = 0.5f;
	private const float EXPECTED_ATTACK_INTERVAL = 0.5f;
	private const int EXPECTED_POTION_HP = 20;
	private const int EXPECTED_SKILL_DAMAGE = 20;

	void Start () {

		GameObject player = GameObject.Find ("Cha_Knight_Single(Clone)");
		float range = player.GetComponent<Sword> ().range;
		//Debug.Log ("Range = " + range);
		float swordDamage = player.GetComponent<Sword> ().damage;
		//Debug.Log ("Damage = " +swordDamage);
		float attackRadius = player.GetComponent<Sword> ().attackRadius;
		//Debug.Log ("Attack Radius = " + attackRadius);
		float healthPoint = player.GetComponent<Player> ().healthPoint;
		//Debug.Log ("Health Point = " + healthPoint);
		float resistance = player.GetComponent<Player> ().resistance;
		//Debug.Log ("Resistance = " + resistance);
		float strength = player.GetComponent<Player> ().strength;
		//Debug.Log ("Strength = " + strength);
		float attackInterval = player.GetComponent<Player> ().attackInterval;
		//Debug.Log ("Attack Interval = " + attackInterval);
		float potionHP = player.GetComponent<UsePotion> ().HP;
		//Debug.Log ("Potion HP = " + potionHP);
		float skillDamage = player.GetComponent<SkillBoom> ().damage;


		bool isRange = range == EXPECTED_SWORD_RANGE;
		bool isSwordDamage = swordDamage == EXPECTED_SWORD_DAMAGE;
		bool isAttackRadius = attackRadius == EXPECTED_SWORD_ATTACK_RADIUS;
		bool isHealthPoint = healthPoint == EXPECTED_HEALTH_POINT;
		bool isResistance = resistance == EXPECTED_RESISTANCE;
		bool isStrength = strength == EXPECTED_STRENGTH;
		bool isAttackInterval = attackInterval == EXPECTED_ATTACK_INTERVAL;
		bool isPotionHP = potionHP == EXPECTED_POTION_HP;
		bool isSkillDamage = skillDamage == EXPECTED_SKILL_DAMAGE;

		bool passed = isRange && isSwordDamage && isAttackRadius && isHealthPoint
		              && isResistance && isStrength && isAttackInterval && isPotionHP
		              && isSkillDamage;

		if (passed) {

			IntegrationTest.Pass ();
			Debug.Log ("Player Initialisation Test Passed");

		} else {

			IntegrationTest.Fail ();
			Debug.Log ("Player Initialisation Test Failed");
		}
	}

}
