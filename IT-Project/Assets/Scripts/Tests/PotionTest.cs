﻿using UnityEngine;
using System.Collections;

public class PotionTest : MonoBehaviour
{

    private GameObject test;
    // Use this for initialization

    void Start()
    {

        GameObject test = GameObject.Find("Cha_Knight_Single(Clone)");

        test.GetComponent<Player>().healthPoint = 80;
        test.GetComponent<UsePotion>().Start();
        test.GetComponent<UsePotion>().Use();

        if (test.GetComponent<Player>().healthPoint == 80 + test.GetComponent<UsePotion>().HP)
        {
            IntegrationTest.Pass();
            Debug.Log("Potion Test Passed");
        }
        else
        {
            IntegrationTest.Fail();
            Debug.Log("Potion Test Failed");
        }
        // Have tried to instantiate a player, but the prefab references would be also destoryed after destorying the instantiated instance.
        // This way is a bit weird, but it works...  
        test.SetActive(false);
        test.SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {

    }
}