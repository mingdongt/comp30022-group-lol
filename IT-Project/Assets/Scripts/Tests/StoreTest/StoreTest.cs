﻿using UnityEngine;
using System.Collections;
using inv;
using System;
/// <summary>
/// The tests focus on checking the changes made by player about the inventory by checking data on the database of server
/// This test case tests if selling and buying item work as expected.
/// </summary>
public class StoreTest : MonoBehaviour {

    private string testPlayerName = "player2";
    private string testPlayerPass = "pass";
    InventorySlots testInv;
    Store testStore;
    private string tempType;
    private string tempLevel;
    // Use this for initialization
    void Start () {
        DBManager.Login(testPlayerName, testPlayerPass);
        cleanSlots();
        testInv = transform.GetComponent<InventorySlots>();
        testStore = transform.GetComponent<Store>();

        if (testInv == null)
        {
            Debug.LogError("Lack of test inventory");
        }

        makeGold();

        testInv.displayRemoteInventory();

        if (testBuyingItem() && testSellingItem())
        {
            IntegrationTest.Pass();
            Debug.Log("Store test passed.");
        }
        else
        {
            IntegrationTest.Fail();
            Debug.Log("Store test failed.");
        }

        cleanSlots();
    }

    private void makeGold()
    {
        // add 1000 gold to the inventory
        DBManager.UpdateInventory(1, 18, 1000);
    }

    private void cleanSlots()
    {
        // clean the item on the first slot in the inventory and character slots.
        DBManager.UpdateInventory(1);
        DBManager.UpdateInventory(2);
    }

    /// <summary>
    ///1. The deserved gold should be added in the inventory after selling item.
    ///2. The item sold should be removed from the inventory.
    /// </summary>
    private bool testSellingItem()
    {
        // sell the item which just bought
        testInv.sellItem(testInv.content[1]);
        // get updated database data
        string[][] inventory = DBManager.GetInventories();
        if (Int32.Parse(inventory[0][11]) == 1000 - testStore.goods[0].price/2 - 1&& inventory[1] ==null)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }


    /// <summary>
    ///1. The gold with amount equal to the price of the good should be removed from inventory.
    ///2. Bought item should be added into the inventory.
    /// </summary>
    private bool testBuyingItem()
    {
        // put items to guarantee there are goods in the store
        testStore.putItems();
        // buy the first item
        testInv.buyItem(testStore.goods[0]);
        // get updated database data
        string[][] inventory = DBManager.GetInventories();
        // check the data 
        if (inventory[1][3] == testStore.goods[0].type && 
            inventory[1][5] == testStore.goods[0].level.ToString() && 
            Int32.Parse(inventory[0][11]) == 1000 - testStore.goods[0].price)
        {
            return true;
        }
        else
        {
            return false;
        }

    } 
}
