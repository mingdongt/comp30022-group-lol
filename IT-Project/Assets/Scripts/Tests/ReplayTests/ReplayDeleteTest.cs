﻿using UnityEngine;
using System.Collections;
using System.IO;


/// <summary>
/// This test is responsible for testing the deleting
/// functionality of the replays.
/// </summary>
public class ReplayDeleteTest : MonoBehaviour {

	private const string TEST_FOLDER = "Test";
	// Use this for initialization
	void Start () {


		bool passedTest = DeleteTest ();

		if (passedTest)
			Debug.Log ("Replay Delete Test Passed");
		else
			Debug.Log ("Replay Delete Test Failed");
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Checks if the dummy test folder is correctly deleted.
	/// </summary>
	/// <returns><c>true</c>, if test was deleted, <c>false</c> otherwise.</returns>
	bool DeleteTest() {

		ReplayButton replayButton = new ReplayButton ();
		DirectoryInfo dir = new DirectoryInfo (Application.persistentDataPath + "/" + TEST_FOLDER);
		dir.Create ();

		bool passed = false;

		replayButton.deleteContentsFromDirectory (TEST_FOLDER);

		if (dir.Exists) {
			passed = false;
		} else {
			passed = true;
		}

		return passed;

	}
}
