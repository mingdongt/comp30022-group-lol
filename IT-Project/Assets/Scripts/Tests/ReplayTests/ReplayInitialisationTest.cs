﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// This test ensures the initialisation of the replay
/// components are done as intended and remain consistent
/// in the development cycle.
/// </summary>
public class ReplayInitialisationTest : MonoBehaviour {


	private const int EXPECTED_MAX_FRAMES = 100;
	// Use this for initialization
	void Start () {

		bool testPassed = InitialisationTest ();

		if (testPassed)
			Debug.Log ("Replay Initialisation Test Passed");
		else
			Debug.Log ("Replay Initialisation Test Failed");
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	/// <summary>
	/// Tests so if the initialisations are done as intended
	/// </summary>
	/// <returns><c>true</c>, if test was initialisationed, <c>false</c> otherwise.</returns>
	bool InitialisationTest() {

		Text recording = GameObject.Find ("Recording").GetComponent<Text>();
		Text pauseToSaveReplay = GameObject.Find ("PauseToSaveReplay").GetComponent<Text>();
		Text replaySaved = GameObject.Find ("ReplaySaved").GetComponent<Text>();
		Capture capture = GameObject.Find ("Recorder").GetComponent<Capture> ();

		int maxFrames = capture.maxFrames;

		bool recordingVisible = !recording.enabled;
		bool pauseToSaveReplayVisible = !pauseToSaveReplay.enabled;
		bool replaySavedVisible = !replaySaved.enabled;
		bool isMaxFramesMatch = maxFrames == EXPECTED_MAX_FRAMES;




		return recordingVisible && pauseToSaveReplayVisible && replaySavedVisible && isMaxFramesMatch;
	}
}
