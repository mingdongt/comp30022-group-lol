﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class tests if a new game session is properly created by checking
/// if a room is generated with a specified room name and a maximum player
/// number. First, if such a room already exists will be tested. Next, the
/// test room is created and then if it has been created properly will be 
/// checked.
/// Note: this test requires some time to complete due to the fact that the 
/// test involves some communications between server. For this reason, when 
/// this test is enabled, please wait for the test result message appear on 
/// the console before selecting the game mode on the title screen. 
/// Otherwise, the player will fail to spawn.
/// </summary>
public class CreateGameSessionTest : MonoBehaviour 
{
	// Test room specifications.
	private int maxnum = 15;
	private string testroom = "TestRoom";
	private const string VERSION = "v1.0.0";
	private bool isEnable = true;

	private bool isRoomThereBeforeTest = false;
	private bool isRoomCreatedWithCorrectNameAndMaxNum = false;

	void Awake ()
	{
		PhotonNetwork.ConnectUsingSettings (VERSION);
	}

	// Creates a new room and join in it. If the room already exists, just join in it.
	private void NewRoom (string roomName, int maxPlayer)
	{
		if (roomName != "" && maxPlayer > 0) 
		{
			PhotonNetwork.JoinOrCreateRoom (roomName, new RoomOptions () { MaxPlayers = (byte) maxPlayer }, null);
		}
	}
	// Checks if the room with the specifications already exist and create the test room.
	void OnJoinedLobby ()
	{
		if (!isEnable)
			return;
		
		CheckIfRoomAlreadyExist ();
		this.NewRoom (testroom, maxnum);
	}

	// When room is created, check if it's created with the correct specifications.
	void OnCreatedRoom ()
	{
		if (!isEnable)
			return;
		
		if (!enabled) return;

		CheckIfRoomCreatedProperly ();

		if (!isRoomThereBeforeTest && isRoomCreatedWithCorrectNameAndMaxNum)
			Debug.Log ("CreateGameSessionTest passed.");
		else
			Debug.LogError ("CreateGameSessionTest failed.");

		PhotonNetwork.Disconnect ();
		this.enabled = false;
		this.isEnable = false;
	}

	// Checks if a room with the same test specifications exists.
	private void CheckIfRoomAlreadyExist ()
	{
		RoomInfo[] rooms = PhotonNetwork.GetRoomList ();
		foreach (RoomInfo room in rooms) 
		{
			if (room.name == testroom && room.maxPlayers == maxnum)
				isRoomThereBeforeTest = true;
		}
	}

	// Checks if the test room is created with the specifications.
	private void CheckIfRoomCreatedProperly ()
	{
		if (PhotonNetwork.room.name == testroom && PhotonNetwork.room.maxPlayers == maxnum)
			isRoomCreatedWithCorrectNameAndMaxNum = true;
	}
}
