﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This test is responsible for testing
/// the prameters of the reviving class,
/// making sure the attributes stay consistent
/// with the expected attributes.
/// </summary>
public class ReviveInitialisationTest : MonoBehaviour {


	private const int EXPECTED_REVIVE_COUNT = 100;
	private const float EXPECTED_REVIVE_RADIUS = 150f;
	Reviving reviving;
	// Use this for initialization
	void Start () {


		reviving = GetComponent<Reviving> ();

		bool passed = InitialisationTest ();

		if (passed)
			Debug.Log ("Revive Intialisation Test Passed");
		else
			Debug.Log ("Revive Initialisation Test Failed");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	bool InitialisationTest()  {

		int revivingCount = reviving.reviveCount;
		float reviveRadius = reviving.reviveRadius;

		bool isRevivingCount = EXPECTED_REVIVE_COUNT == revivingCount;
		bool isReviveRadius = EXPECTED_REVIVE_RADIUS == reviveRadius;

		return isRevivingCount && isReviveRadius;
	}
}
