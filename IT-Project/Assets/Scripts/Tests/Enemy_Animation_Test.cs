﻿using UnityEngine;
using System.Collections;


/// <summary>
/// This is test is to ensure the enemy animation operates properly
/// it checks if the attack, damage, dead, wait and walk animations
/// operate properly in both multiplayer and singleplayer.
/// </summary>
public class Enemy_Animation_Test : MonoBehaviour {

	public Transform enemyTestSpawn;
	// Use this for initialization
	void Start () {

		GameObject enemy;
		if (GameManager.Instance.singleplay)
			enemy = (GameObject)Instantiate (Resources.Load (GameManager.Instance.singleEnemyPrefabName), enemyTestSpawn.position, enemyTestSpawn.rotation);
		else
			enemy = (GameObject)Instantiate (Resources.Load (GameManager.Instance.multiEnemyPrefabName), enemyTestSpawn.position, enemyTestSpawn.rotation);
		Animation anim = enemy.GetComponent<Animation> ();

		bool attack = anim.Play ("Attack");
		bool damage = anim.Play ("Damage");
		bool dead = anim.Play ("Dead");
		bool wait = anim.Play ("Wait");
		bool walk = anim.Play ("Walk");

		bool animationWorking = attack && damage && dead && wait && walk;

		if (animationWorking) {

			IntegrationTest.Pass ();
			Debug.Log ("Enemy Animation Test Passed");
		} else {

			IntegrationTest.Fail ();
			Debug.Log ("Enemy Animation Test Failed");
		}

		Destroy (enemy);

	}
	

}
