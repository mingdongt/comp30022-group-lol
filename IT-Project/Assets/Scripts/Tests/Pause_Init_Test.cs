﻿using UnityEngine;
using System.Collections;

/// <summary>
/// test checks that pause works by pausing at the start,
/// checking that all the ui buttons are disabled because of pause,
/// and that the paused boolean is true,
/// then unpausing the game
/// </summary>
public class Pause_Init_Test : MonoBehaviour {

    private bool passed;

	void Start () {

        GameObject pauseButtonHost = GameObject.Find("pauseButtonHost");
        PauseButton pauseButton = pauseButtonHost.GetComponent<PauseButton>();
  
        GameObject attackButton = GameObject.Find("AttackButton");
        GameObject skillButton1 = GameObject.Find("SkillBoomButton");
        GameObject potionButton = GameObject.Find("PotionButton");
        GameObject joystick = GameObject.Find("JoystickBackgroundImage");
        GameObject hpBar = GameObject.Find("Slider");
        pauseButton.pause();
        bool check = pauseButton.paused == true;
        bool aCheck = attackButton.activeInHierarchy == false;
        bool sCheck = skillButton1.activeInHierarchy == false;
        bool pCheck = potionButton.activeInHierarchy == false;
        bool jCheck = joystick.activeInHierarchy == false;
        bool hCheck = hpBar.activeInHierarchy == false;

        passed = check && aCheck && sCheck && pCheck && jCheck && hCheck;
        pauseButton.pause();

        if (passed) {

            IntegrationTest.Pass();
            Debug.Log("Pause Initialisation Test Passed");

        }
        else {

            IntegrationTest.Fail();
            Debug.Log("Pause Initialisation Test Failed");
        }
    }
	

}
