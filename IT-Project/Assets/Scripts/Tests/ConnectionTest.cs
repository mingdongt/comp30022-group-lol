﻿using UnityEngine;
using System.Collections;

public class ConnectionTest : MonoBehaviour {


	void Start () {


		 //This test is responsible for checking if the multiplayer is able to connect to
		 //the Photon cloud server

		bool connected = this.GetComponentInChildren <NetworkManager> ().Connected;

		if (connected == true) {
			IntegrationTest.Pass ();
			Debug.Log ("Connection Test Passed: Connection Established: " + PhotonNetwork.ServerAddress);
		} else {
			IntegrationTest.Fail ();
			Debug.Log ("Connection Test Failed: Connection Failed");
		}
	
	}



}
