﻿using UnityEngine;
using System.Collections;
using inv;
/// <summary>
/// The tests focus on checking the changes made by player about the inventory by checking data on the database of server
/// This test case tests if equipping and unequipping item work as expected.
/// </summary>
public class InventoryTest : MonoBehaviour {


    private string testPlayerName = "player2";
    private string testPlayerPass = "pass";
    InventorySlots testInv;
    CharacterSlots testCha;
    private string tempType;
    private string tempLevel;

    // Use this for initialization
    void Start () {

        DBManager.Login(testPlayerName, testPlayerPass);
        cleanSlots();
        testInv = transform.GetComponent<InventorySlots>();
        testCha = transform.GetComponent<CharacterSlots>();
        
        if (testInv == null)
        {
            Debug.LogError("Lack of test inventory");
        }
        if (testCha == null)
        {
            Debug.LogError("Lack of test character slots");
        }
        makeTestEquipment();
        // update the inventory
        testInv.displayRemoteInventory();

        // save the data before equipping
        tempType = testInv.content[0].type;
        tempLevel = testInv.content[0].level.ToString();

        if (testEquipItem() && testUnequipItem())
        {
            IntegrationTest.Pass();
            Debug.Log("Inventory system test passed.");
        }
        else
        {
            IntegrationTest.Fail();
            Debug.Log("Inventory system test failed.");
        }

        cleanSlots();
        
    }

    private void makeTestEquipment()
    {
        // add a weapon to the first slof of inventory
        DBManager.UpdateInventory(1, 1, 1);
    }

    private void cleanSlots()
    {
        // clean the item on the first slot in the inventory and character slots.
        DBManager.UpdateInventory(1);
        DBManager.UpdateEquipment(1);
    }

    /// <summary>
    ///1. The item in the corresponded character slot should be same with the item equipped. 
    ///2. The item equipped should be removed from the inventory
    /// </summary>
    private bool testEquipItem()
    {

        testCha.equipItem(testInv.content[0]);

        // get the data after equipping
        string[][] equipments = DBManager.GetEquipments();
        string[][] inventory = DBManager.GetInventories();
        if (equipments[0][3] == tempType && equipments[0][5] == tempLevel && inventory[0] == null)
        {
            return true;  
        }
        else
        {        
            return false;
        }
    }

    /// <summary>
    ///1. The item in the corresponded character slot should same with the weapon unequipped. 
    ///2. The item unequipped should be removed from the character slots.
    /// </summary>
    private bool testUnequipItem()
    {
        testCha.unequipItem(testCha.slot[0], 1);

        // get the data after equipping
        string[][] equipments = DBManager.GetEquipments();
        string[][] inventory = DBManager.GetInventories();
        if (inventory[0][3] == tempType && inventory[0][5] == tempLevel && equipments[0] == null)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

}
