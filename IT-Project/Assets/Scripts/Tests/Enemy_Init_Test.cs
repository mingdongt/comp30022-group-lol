﻿using UnityEngine;
using System.Collections;


/// <summary>
/// This test is intended to make sure the intended attributes are
/// in line with the actual attributes of the enemies, this way there
/// is no discrepancies. At any time a attribute is changed the test will
/// fail and inform the user.
/// </summary>
public class Enemy_Init_Test : MonoBehaviour {

	public Transform enemyTestSpawn;
	private const float EXPECTED_HEALTH_POINT = 50f;
	private const float EXPECTED_RESISTANCE = 0.2f;
	private const float EXPECTED_STRENGTH = 0.5f;
	private const float EXPECTED_ATTACK_INTERVAL = 0.5f;
	private const float EXPECTED_RANGE = 80f;
	private const float EXPECTED_DAMAGE = 100f;
	private const float EXPECTED_ATTACK_RADIUS = 20f;


	// Use this for initialization
	void Start () {
		
		//Instantiate (Resources.Load ("Cha_Slime2"), enemyTestSpawn.position, enemyTestSpawn.rotation);
		InitialisationTest ();

	}


	void InitialisationTest() {
		
		GameObject enemy;

		if (GameManager.Instance.singleplay)
			enemy = (GameObject)Instantiate (Resources.Load (GameManager.Instance.singleEnemyPrefabName), enemyTestSpawn.position, enemyTestSpawn.rotation);
		else
			enemy = (GameObject)Instantiate (Resources.Load (GameManager.Instance.multiEnemyPrefabName), enemyTestSpawn.position, enemyTestSpawn.rotation);
		
		//GameObject enemy = GameObject.FindGameObjectWithTag ("enemy");
		float healthPoint = enemy.GetComponent<Enemy> ().healthPoint;
		float resistance = enemy.GetComponent<Enemy> ().resistance;
		float strength = enemy.GetComponent<Enemy> ().strength;
		float attackInterval = enemy.GetComponent<Enemy> ().attackInterval;
		float range = enemy.GetComponent<Barehand> ().range;
		float damage = enemy.GetComponent<Barehand> ().damage;
		float attackRadius = enemy.GetComponent<Barehand> ().attackRadius;


		bool isHealthPoint = healthPoint == EXPECTED_HEALTH_POINT;
		bool isResistance = resistance == EXPECTED_RESISTANCE;
		bool isStrength = strength == EXPECTED_STRENGTH;
		bool isAttackInterval = attackInterval == EXPECTED_ATTACK_INTERVAL;
		bool isRange = range == EXPECTED_RANGE;
		bool isDamage = damage == EXPECTED_DAMAGE;
		bool isAttackRadius = attackRadius == EXPECTED_ATTACK_RADIUS;
		bool passed = isHealthPoint && isResistance && isStrength && isAttackInterval 
			&& isRange && isDamage && isAttackRadius;
		
		if (passed) {

			IntegrationTest.Pass ();
			Debug.Log ("Enemy Initialisation Test Passed");

		} else {
			IntegrationTest.Fail ();
			Debug.Log ("Enemy Initialisation Test Failed");
		}

		Destroy (enemy);

	}
	

}
