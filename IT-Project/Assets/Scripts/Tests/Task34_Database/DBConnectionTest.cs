﻿using UnityEngine;
using System.Collections;
using MySql.Data.MySqlClient;

/// <summary>
/// This test case tests if:
/// 1. login success with correct player name and password
/// 2. login fails with incorrect player name and password
/// 3. SSL connection is enabled when it is needed
/// 4. SSL connection is disabled when it is not needed
/// </summary>
public class DBConnectionTest : MonoBehaviour 
{
	private bool loginTest = false;
	private bool normalSSLTest = false;
	private bool secureSSLTest = false;
	private string testPlayerName = "TestPlayer";
	private string testPlayerPass = "password";
	private string testPlayerWrongPass = "wrongpassword";
	public bool DEBUG = false;	// on/off or debug messages

	void Awake ()
	{
		try 
		{
			loginTest = LoginTest ();
			normalSSLTest = SSLTest (true);
			secureSSLTest = SSLTest (false);

			if (loginTest && normalSSLTest && secureSSLTest)
				Debug.Log ("DBConnectionTest passed");
			else
				Debug.LogError ("DBConnectionTest failed " +loginTest+ " " +normalSSLTest+ " " +secureSSLTest);
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("DBConnectionTest failed.\n" + ex);
			return;
		}

		Debug.Log ("DBConnectionTest passed.");
	}


	// Login Test.
	// Assumption: player namde and password are stored in the database.
	// Successful login is assumed and if the login fails, it returns false.
	// Otherwise it will return true;
	private bool LoginTest ()
	{
		return (DBManager.Login (testPlayerName, testPlayerPass) && 
				!DBManager.Login (testPlayerName, testPlayerWrongPass));
	}

	// Checks the type of cipher used in the current connection.
	// If SSL is not used and the cipher string is empty, true will be returned.
	// If SSL is used and the cipher string is not empty, true will be returned.
	// Otherwise, false will be returned.
	public bool SSLTest (bool isSecure)
	{
		string conn_string = "";
		string cipher = null;
		string query = "SHOW STATUS like 'Ssl_cipher';";
		if (isSecure)
			conn_string = DBManager.GetSecureConnectionString ();
		else
			conn_string = DBManager.GetConnectionString ();
		try 
		{
			using (MySqlConnection conn = new MySqlConnection (conn_string)) 
			{
				using (MySqlCommand cmd = new MySqlCommand (query, conn)) 
				{
					conn.Open ();
					using (MySqlDataReader reader = cmd.ExecuteReader ()) 
					{
						while (reader.HasRows && reader.Read ()) 
						{
							cipher = reader.GetString (1);
							if (DEBUG)
								Debug.Log ("SSL cipher (secure=" + isSecure + "): " + cipher);
						}
					}
				}
			}
			if (isSecure && cipher.Length > 0)
				return true;
			else if (!isSecure && cipher.Length == 0)
				return true;
			else
				return false;
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in GetSSLCipher :\n" + ex);
			return false;
		}
	}
}
