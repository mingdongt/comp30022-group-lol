﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This test case tests if password is hashed properly using a salt:
/// 	1. Checks if correct password & salt combination generates the identical password hash.
/// 	2. Checks if incorrect password & salt combination generates a different password hash.
/// Test is conducted for a specified number of time.
/// </summary>
public class SaltFunctionalityTest : MonoBehaviour 
{
	public int testIteration = 1;
	void Awake ()
	{
		for (int i = 0; i < testIteration; i++) 
		{
			string pass1 = "password1";
			string pass2 = "password2";
			byte[] salt1 = PasswordHashing.GenerateSalt ();
			byte[] salt2 = PasswordHashing.GenerateSalt ();
			byte[] hashed1 = PasswordHashing.GenerateHashedPassword (pass1, salt1);
			byte[] hashed2 = PasswordHashing.GenerateHashedPassword (pass2, salt2);

			if (!PasswordHashing.VerifyPassword (pass1, hashed1, salt1) || !PasswordHashing.VerifyPassword (pass2, hashed2, salt2)) {
				Debug.LogError ("SaltFunctionalityTest failed: verifying password returned where two password hashes should be identical.");
				return;
			}

			if (PasswordHashing.VerifyPassword (pass1, hashed1, salt2) || PasswordHashing.VerifyPassword (pass2, hashed2, salt1)) {
				Debug.LogError ("SaltFunctionalityTest failed: wrong salt generated a correct password.");
				return;
			}
		}
		Debug.Log ("SaltFunctionalityTest passed.");
	}
}
