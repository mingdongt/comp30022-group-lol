﻿using UnityEngine;
using System.Collections;
using MySql.Data.MySqlClient;

/// <summary>
/// This test case checks if each database API is properly working.
/// Accessing database manually is required in order to confirm 
/// if the intended results are found.
/// </summary>
public class DBFunctionsTest : MonoBehaviour 
{
	private string [][] inventories = null;
	private string [][] items = null;
	private string [][] equips = null;
	private int [] stats = null;
	public bool DEBUG = false;	// on/off or debug messages

	void Start ()
	{
		try 
		{
			ListInventoryTest ();
			ListItemsTest ();
			ListEquipmentsTest ();
			UpdateEquipmentTest ();
			UpdateInventoryTest ();
			GetPlayerExpTest ();
			UpdateEXPTest ();
			BuyItemTest ();
			GetStatisticsTest ();
			UpdateStatisticsTest ();
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("DBFunctionsTest failed.\n" + ex);
			return;
		}

		Debug.Log ("DBFunctionsTest found no errors. Refer to the tables directly to check the correctness.");
	}

	// Checks if inventory_slot corresponds to the array index.
	// Also checks it there is any error in the process.
	private bool ListInventoryTest ()
	{
		// Update the inventory. slot_no, item_no.
		DBManager.UpdateInventory (7, 5);

		// Get the inventory list.
		inventories = DBManager.GetInventories ();

		for (int i=0; i<DBManager.INVENTORY_SLOTS; i++)
		{
			if (DEBUG)
				Debug.Log ("------------Inventory slot: " +(i+1) +"------------");

			if (inventories[i] == null)
				continue;

			if (DEBUG) 
			{
				Debug.Log ("inventory_slot\t: " + inventories [i] [0]);
				Debug.Log ("item_id\t: " + inventories [i] [1]);
				Debug.Log ("item_name\t" + inventories [i] [2]);
				Debug.Log ("type_name\t: " + inventories [i] [3]);
				Debug.Log ("price\t: " + inventories [i] [4]);
				Debug.Log ("level\t: " + inventories [i] [5]);
				Debug.Log ("armour\t: " + inventories [i] [6]);
				Debug.Log ("attack\t: " + inventories [i] [7]);
				Debug.Log ("speed\t: " + inventories [i] [8]);
				Debug.Log ("amount\t: " + inventories [i] [9]);
				Debug.Log ("max_amount\t: " + inventories [i] [10]);
				Debug.Log ("quantity\t: " + inventories [i] [11]);
			}
		}

		return true;
	}
		
	// Checks it there is any error in the process.
	private bool ListItemsTest ()
	{
		// Get the inventory list.
		items = DBManager.GetItems ();

		for (int i=0; i<items.Length; i++)
		{
			if (DEBUG)
				Debug.Log ("------------ item: " +(i+1) +"------------");

			if (items[i] == null)
				continue;
			
			if (DEBUG) 
			{
				Debug.Log ("item_id\t: " + items [i] [0]);
				Debug.Log ("item_name\t" + items [i] [1]);
				Debug.Log ("type_name\t: " + items [i] [2]);
				Debug.Log ("price\t: " + items [i] [3]);
				Debug.Log ("level\t: " + items [i] [4]);
				Debug.Log ("armour\t: " + items [i] [5]);
				Debug.Log ("attack\t: " + items [i] [6]);
				Debug.Log ("speed\t: " + items [i] [7]);
				Debug.Log ("amount\t: " + items [i] [8]);
				Debug.Log ("max_amount\t: " + items [i] [9]);
			}
		}

		return true;
	}

	// Checks if inventory_slot corresponds to the array index.
	// Also checks it there is any error in the process.
	private bool ListEquipmentsTest ()
	{
		// Get the inventory list.
		equips = DBManager.GetEquipments ();

		for (int i = 0; i < equips.Length; i++) {
			if (DEBUG)
				Debug.Log ("------------ Equipment: " + (i + 1) + "------------");

			if (equips [i] == null)
				continue;

			if (DEBUG)
			{
				Debug.Log ("equipment_slot\t: " + equips [i] [0]);
				Debug.Log ("item_id\t: " + equips [i] [1]);
				Debug.Log ("item_name\t" + equips [i] [2]);
				Debug.Log ("type_name\t: " + equips [i] [3]);
				Debug.Log ("price\t: " + equips [i] [4]);
				Debug.Log ("level\t: " + equips [i] [5]);
				Debug.Log ("armour\t: " + equips [i] [6]);
				Debug.Log ("attack\t: " + equips [i] [7]);
				Debug.Log ("speed\t: " + equips [i] [8]);
				Debug.Log ("amount\t: " + equips [i] [9]);
				Debug.Log ("max_amount\t: " + equips [i] [10]);
			}
		}

		return true;
	}

	// Test for updating Equipment table.
	private void UpdateEquipmentTest ()
	{
		// slot_no, item_no
		DBManager.UpdateEquipment (4, 1);
		//DBManager.UpdateEquipment (4);
		//DBManager.UpdateEquipment (5);
	}

	// Test for updating Inventory table.
	private void UpdateInventoryTest ()
	{
		// slot_no, item_no
		// Slot 6 should have item 5 with quantity 1.
		DBManager.UpdateInventory (6, 5);
		// Should be removed and quantity set to 0.
		DBManager.UpdateInventory (6);
		// Should not cause any error.
		DBManager.UpdateInventory (6);
		// Now there should be item 3 in slot 6 with quantity 1.
		DBManager.UpdateInventory (6, 3);
		// Now the quantity should be 2.
		DBManager.UpdateInventory (6, 3);
		// Now the quantity should be 1.
		DBManager.UpdateInventory (6, 3, -1);
		// Now the quantity should be 0 and item_id should be NULL.
		DBManager.UpdateInventory (6, 3, -1);
		// There should be no change from the previous result.
		DBManager.UpdateInventory (6, 3, -1);
		// There should be no change from the previous result.
		DBManager.UpdateInventory (6, 3, -100);
	}

	// Test for getting player's experience point.
	private void GetPlayerExpTest ()
	{
		if (DEBUG)
			Debug.Log ("EXP: " + DBManager.GetPlayerEXP ());
	}

	// Tests for updating experience point.
	private void UpdateEXPTest ()
	{
		// Should be added by 100.
		DBManager.UpdateEXP (100);
		// Should be set to 50.
		//DBManager.UpdateEXP (10, 50);
		// Should be set to 0.
		//DBManager.UpdateEXP (10, 0);
		// Should not change.
		//DBManager.UpdateEXP (-100, -1);
	}

	// Test for buying item.
	private void BuyItemTest ()
	{
		// item_id, slot_no.
		DBManager.BuyItem (1, 10);
	}

	// Test for getting statistics of the player.
	private void GetStatisticsTest ()
	{
		stats = DBManager.GetStatistics ();
		if (DEBUG) 
		{
			Debug.Log ("------------ Statistics------------");
			Debug.Log ("num_defeat\t: " + stats [0]);
			Debug.Log ("num_boss_defeat\t: "	+ stats [1]);
			Debug.Log ("max_rounds\t: " + stats [2]);
			Debug.Log ("num_downed\t: " + stats [3]);
			Debug.Log ("num_revived\t: " + stats [4]);
			Debug.Log ("num_get_revived\t: " + stats [5]);
		}
	}

	// Test for updating statistics.
	private void UpdateStatisticsTest ()
	{
		DBManager.UpdateStatistics (100, 3, 10, 5, 3, 0);
	}
}
