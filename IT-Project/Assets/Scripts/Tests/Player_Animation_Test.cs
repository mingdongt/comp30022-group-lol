﻿using UnityEngine;
using System.Collections;


//This Test is intended to test the player animation component
//It checks if the animations Attack, Walk, Wait, Dead, and Damage
//are working properly. The Test will fail if one of the animations
//fails to run.

public class Player_Animation_Test : MonoBehaviour {

	void Start () {

		GameObject player;


		player = GameObject.Find ("Cha_Knight_Single(Clone)");
	

		Animation anim = player.GetComponent<Animation> ();

		bool attack = anim.Play ("Attack");
		bool walk = anim.Play ("Walk");
		bool wait = anim.Play ("Wait");
		bool dead = anim.Play ("Dead");
		bool damage = anim.Play ("Damage");

		bool animationWorking = attack && walk && wait && dead && damage;

		if (animationWorking) {

			IntegrationTest.Pass ();
			Debug.Log ("Player Animation Test Passed");
		} else {

			IntegrationTest.Fail ();
			Debug.Log ("Player Animation Test Failed");
		}


	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
