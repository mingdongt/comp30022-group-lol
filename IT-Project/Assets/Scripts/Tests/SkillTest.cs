﻿using UnityEngine;
using System.Collections;

public class skillTest : MonoBehaviour
{

    private GameObject test;
    // Use this for initialization

    void Start()
    {

        GameObject test = GameObject.Find("Cha_Knight_Single(Clone)");
        GameObject enemy = (GameObject)Instantiate(Resources.Load("Cha_Slime2"));
        test.GetComponent<SkillBoom>().Start();
        enemy.GetComponent<Enemy>().Start();
        float enemyStartHealth = enemy.GetComponent<Enemy>().healthPoint;
        test.GetComponent<SkillBoom>().cast();


        if (enemy.GetComponent<Enemy>().healthPoint < enemyStartHealth)
        {
            IntegrationTest.Pass();
            Debug.Log("Skill Boom Test Passed");
        }
        else
        {
            IntegrationTest.Fail();
            Debug.Log("Skill Boom Test Failed");
        }
        test.SetActive(false);
        test.SetActive(true);
        Destroy(enemy);

    }

    // Update is called once per frame
    void Update()
    {

    }
}