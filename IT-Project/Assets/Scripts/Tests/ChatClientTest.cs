﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Threading;

public class ChatClientTest : MonoBehaviour {


	TcpClient tcpclient;
	Stream stm;
	ASCIIEncoding asen;
	public List<string> chatHistory;
	private string currentMessage = string.Empty;
	private System.Object lockThis = new System.Object ();
	Thread t;
	ChatClient chatClient;


	// Use this for initialization
	void Start () {

		chatClient = new ChatClient ();

		bool testPassed = ConnectionTest ();

		if (testPassed) {
			Debug.Log ("Chat Connection Test Passed");
		} else {
			Debug.Log ("Chat Connection Test Failed");
		}
	}
	

	private bool ConnectionTest() {

		bool connected = chatClient.ConnectChatClient ();

		if (connected) {
			return true;
		}

		return false;
	}
}
