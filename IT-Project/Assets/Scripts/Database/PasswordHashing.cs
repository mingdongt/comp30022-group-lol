﻿using System;
using UnityEngine;
using System.Collections;
using System.Security.Cryptography;

/// <summary>
/// This class provides functionalities related to hashing password as well as 
/// comparing two hashes to see if they are identical.
/// PBKDF2 is used as hashing algorithm.
/// </summary>
public class PasswordHashing
{
	private const int SaltLength = 32;
	private const int HashLength = 32;
	private const int IterationCount = 10000; // least number recommended by NIST.

	// Generates salt using the specified byte length.
	public static byte[] GenerateSalt (int saltByteSize = SaltLength)
	{
		// RNGCryptoServiceProvider provides a cryptographically secure rand # generation. 
		RNGCryptoServiceProvider csprng = new RNGCryptoServiceProvider ();
		byte[] salt = new byte [saltByteSize];
		csprng.GetBytes (salt);
		return salt;
	}

	// Uses PBKDF2 to generate hash
	public static byte[] GenerateHashedPassword (string password, byte[] salt, int iCount = IterationCount, int hashByteSize = HashLength)
	{
		// Hashes the password using salt, applying the pseudo-random function for the specified # of times.
		Rfc2898DeriveBytes hashGenerator = new Rfc2898DeriveBytes (password, salt);
		hashGenerator.IterationCount = iCount;
		return hashGenerator.GetBytes (hashByteSize);
	}

	// Compute hash using the password that a user has input, and compare the hash with the one stored in DB.
	public static bool VerifyPassword (String password, byte[] hashFromDB, byte[] saltFromDB )
	{
		byte[] hashFromInput = GenerateHashedPassword (password, saltFromDB);
		return CompareHashes (hashFromInput, hashFromDB);
	}

	// Compare two hashes to check if they are identical.
	private static bool CompareHashes (byte[] hashA, byte[] hashB)
	{
		// Length of two hashes must be the same.
		if (hashA.Length != hashB.Length)
			return false;
		
		for (int i = 0; i < hashA.Length; i++) 
		{
			if (hashA [i] != hashB [i])
				return false;
		}

		return true;
	}

	// Converts byte[] to string.
	public static string ConvertByteToString (byte[] toBeConverted)
	{
		return Convert.ToBase64String (toBeConverted);
	}

	// Converts string to byte[].
	public static byte[] ConvertStringToByte (string toBeConverted)
	{
		return Convert.FromBase64String (toBeConverted);
	}

}
