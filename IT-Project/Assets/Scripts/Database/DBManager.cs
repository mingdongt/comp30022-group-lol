﻿using System;
using System.Data;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

/// <summary>
/// This class provides a connection to DB server and APIs of various queries to the client.
/// </summary>
public class DBManager
{
	// DB Connection information.
	private static string connString = null;
	private const string SERVER = "130.56.255.92";
	private const uint PORT = 8080;
	private const string DATABASE = "itproject_LOL";

	public static int playerId; // primary key for the Player table.
	public const int EQUIPMENT_SLOTS = 5;	// Number of slots in the equipment table.
	public const int INVENTORY_SLOTS = 12; // Number of slots in the inventory table.
	public const int ITEM_COLS = 10; // Number of columns in the item table.
	public const int STATISTICS_COLS = 6; // Number of colums of Statistics table that are used.

	// User name and password.
	private static string user = "";
	private static string pass = "";

	private DBManager () {}

	/// <summary>
	/// Returns a connection string used to make a connection to DB server.
	/// </summary>
	/// <returns>The connection string.</returns>
	public static string GetConnectionString ()
	{
		if (connString != null)
			return connString;
		
		return BuildConnectString (false).ToString ();
	}
		
	/// <summary>
	/// Returns a connection string used to make a secure connection to DB server.
	/// </summary>
	/// <returns>The connection string.</returns>
	public static string GetSecureConnectionString ()
	{
		if (connString != null)
			return connString;

		return BuildConnectString (true).ToString ();
	}

	/// <summary>
	/// Used to build the connection string used to establish the connection to DB server.
	/// </summary>
	/// <returns>The connect string.</returns>
	/// <param name="isSecure">If set to <c>true</c> the secure mode (ssl enable) is used.</param>
	private static MySqlConnectionStringBuilder BuildConnectString (bool isSecure)
	{
		MySqlConnectionStringBuilder conn_string = new MySqlConnectionStringBuilder ();
		conn_string.Server = SERVER;
		conn_string.Server = SERVER;
		conn_string.Port = PORT;
		conn_string.UserID = user;
		conn_string.Password = pass;
		conn_string.Database = DATABASE;
		if (isSecure)
			conn_string.SslMode = MySqlSslMode.Required;
		else
			conn_string.SslMode = MySqlSslMode.None;
		return conn_string;
	}


	/// <summary>
	/// Inserts the player: Add a new player to the database. Used for the development purposes.
	/// </summary>
	/// <returns><c>true</c>, if player was inserted, <c>false</c> otherwise.</returns>
	/// <param name="playerName">Player name.</param>
	/// <param name="pass">User password.</param>
	/// <param name="email">User email address.</param>
	public static bool InsertPlayer (string playerName, string pass, string email)
	{
		// Hashing the password using a salt.
		byte[] salt = PasswordHashing.GenerateSalt ();
		string saltStr = PasswordHashing.ConvertByteToString (salt);
		string passStr = PasswordHashing.ConvertByteToString (PasswordHashing.GenerateHashedPassword (pass, salt));

		string query =
			"INSERT INTO Player (player_id, player_name, password, salt, email, member_date) VALUES (" + 
				"NULL,'" + playerName + "','" + passStr + "','"+ saltStr + "','" + email + "', NOW());";
		try 
		{
			using (MySqlConnection conn = new MySqlConnection (GetSecureConnectionString ())) 
			{
				using (MySqlCommand cmd = new MySqlCommand (query, conn)) 
				{
					conn.Open ();
					cmd.ExecuteNonQuery ();
				}
			}
			return true;
		} 
		catch (MySqlException ex) 
		{
			if (ex.Number == 1062)
				Debug.Log ("The player name and email combination is already resistered.");
			else
				Debug.Log ("Error in InsertPlayer:\n" +ex);
			return false;
		}
	}

	/// <summary>
	/// Removes an existing player from the database.
	/// </summary>
	/// <returns><c>true</c>, if player was removed, <c>false</c> otherwise.</returns>
	/// <param name="playerName">Player name used as a primary key.</param>
	public static bool RemovePlayer (string playerName)
	{
		string query =
			"DELETE FROM Player WHERE player_name = @name;";

		try {
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{
				using (MySqlCommand cmd = new MySqlCommand (query, conn)) 
				{
					conn.Open ();
					cmd.Parameters.AddWithValue ("@name", playerName);
					cmd.ExecuteNonQuery ();
				}
			}
			return true;
		} 
		catch (MySqlException ex) 
		{
			Debug.Log ("Error in RemovePlayer:\n" +ex);
			return false;
		}
	}

	/// <summary>
	/// Checks if login was successful by verifying player name and password.
	/// True is returned if the login was successful; False otherwise.
	/// </summary>
	/// <param name="playerName">Player name.</param>
	/// <param name="password">Password.</param>
	public static bool Login (string playerName, string password)
	{
		string id = "";
		string passwordFromDB = "";
		string saltFromDB = "";
		string query = "SELECT player_id, password, salt FROM Player WHERE player_name=@player";

		// Set user info.
		user = playerName;
		pass = password;

		// Debug.Log ("Login: " + user + " " + pass);

		try 
		{
			string conn_string = GetSecureConnectionString ();
			using (MySqlConnection conn = new MySqlConnection (conn_string)) 
			{
				using (MySqlCommand cmd = new MySqlCommand (query, conn)) 
				{
					conn.Open ();
					cmd.Parameters.AddWithValue ("@player", playerName);
					cmd.Prepare ();
					using (MySqlDataReader reader = cmd.ExecuteReader ()) 
					{
						while (reader.HasRows && reader.Read ()) 
						{
							id = reader.GetString (0);
							passwordFromDB = reader.GetString (1);
							saltFromDB = reader.GetString (2);
						}
						// Stores player id for later reference.
						if (id != "")
							playerId = int.Parse (id);
						else
							return false;
						// Verifies the user inputs.
						if (PasswordHashing.VerifyPassword (password, 
							PasswordHashing.ConvertStringToByte(passwordFromDB), 
							PasswordHashing.ConvertStringToByte(saltFromDB)))
						{
							// Insert a row into the history table.
							AddLoginHistory ();
							return true;
						}
					}
				}
			}
		}
		catch (MySqlException ex) 
		{
			if (ex.Number == 0)
				Debug.Log ("Login failure");
			else
				Debug.LogError ("Error in Login :\n" + ex);
		}
		return false;
	}

	/// <summary>
	/// Used to add a login history. Automatically called after a player has logged in.
	/// </summary>
	/// <returns><c>true</c>, if login history was successfully added, <c>false</c> otherwise.</returns>
	private static bool AddLoginHistory ()
	{
		string query = "INSERT INTO Login_history (player_id, login_time) VALUES (@id, NOW());";
		try 
		{
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{
				using (MySqlCommand cmd = new MySqlCommand (query, conn)) 
				{
					conn.Open ();
					cmd.Parameters.AddWithValue ("@id", playerId);
					cmd.Prepare ();
					cmd.ExecuteReader ();
				}
			}
			return true;
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in AddLoginHistory :\n" + ex);
			return false;
		}
	}
		
	/// <summary>
	/// Used to add a logout time in the login history table.
	/// </summary>
	/// <returns><c>true</c>, if logout history was added, <c>false</c> otherwise.</returns>
	public static bool AddLogoutHistory ()
	{
		string query = "UPDATE Login_history SET logout_time = NOW() " +
			"WHERE player_id = @id ORDER BY login_history_id DESC LIMIT 1;";

		try 
		{
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{
				using (MySqlCommand cmd = new MySqlCommand (query, conn)) 
				{
					conn.Open ();
					cmd.Parameters.AddWithValue ("@id", playerId);
					cmd.Prepare ();
					cmd.ExecuteReader ();
				}
			}
			return true;
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in AddLogoutHistory :\n" + ex);
			return false;
		}
	}
		
	/// <summary>
	/// Returns the player's experience point.
	/// </summary>
	/// <returns>The player's experience point.</returns>
	public static int GetPlayerEXP ()
	{
		string ex_point = "";
		int ex_point_int = -1;
		string query = "SELECT experience FROM Player WHERE player_id = @player_id;";

		try 
		{
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{
				using (MySqlCommand cmd = new MySqlCommand (query, conn)) 
				{
					conn.Open ();
					cmd.Parameters.AddWithValue ("@player_id", playerId);
					using (MySqlDataReader reader = cmd.ExecuteReader ()) 
					{
						while (reader.HasRows && reader.Read ()) 
						{
							ex_point = reader.GetString (0);
						}
					}
				}
			}
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in GetPlayerExp :\n" + ex);
			return -1;
		}

		if (ex_point == "" || !int.TryParse (ex_point, out ex_point_int))
			return -1;
		return ex_point_int;
	}
		
	/// <summary>
	/// Returns the price of a specified item.
	/// </summary>
	/// <returns>The price.</returns>
	/// <param name="itemId">Item identifier.</param>
	public static int GetPrice (int itemId)
	{
		int intPrice = 0;
		string price = "";
		string query = "SELECT price FROM Item WHERE item_id = @item_id;";

		try 
		{
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{
				using (MySqlCommand cmd = new MySqlCommand (query, conn)) 
				{
					conn.Open ();
					cmd.Parameters.AddWithValue ("@item_id", itemId);
					using (MySqlDataReader reader = cmd.ExecuteReader ()) 
					{
						while (reader.HasRows && reader.Read ()) 
						{
							price = reader.GetString (0);
						}
					}
				}
			}
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in GetPrice :\n" + ex);
			return -1;
		}

		if (price == "" || !int.TryParse (price, out intPrice))
			return -1;
		return intPrice;
	}
		
	/// <summary>
	/// Lists all the items available in the world.
	/// </summary>
	/// <returns>All the items in the Item table in the database.
	/// Double array is returned whose each row represents each item that contains the item attributes.</returns>
	public static string [][] GetItems ()
	{
		List <string []> items = new List <string[]> (); 
		string query = 
			"SELECT item_id, item_name, item_type_name, price, level, " +
			"armor, attack, speed, amount, max_amount " +
			"FROM Item, Item_type WHERE Item.item_type_id = Item_type.item_type_id;";
		try 
		{
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{
				using (MySqlCommand cmd = new MySqlCommand (query, conn)) 
				{
					conn.Open ();
					using (MySqlDataReader reader = cmd.ExecuteReader ()) 
					{
						while (reader.HasRows && reader.Read ()) 
						{
							string [] row;
							row = ReadItems (reader, 10);
							items.Add (row);
						}
					}
				}
			}
		}

		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in ListItems :\n" + ex);
			return null;
		}
		return items.ToArray ();
	}
		
	/// <summary>
	/// Lists all the items that the player currently holds.
	/// </summary>
	/// <returns>All items in the current player's inventory.
	/// Double arrayis returned whose each row represents each item in the inventory's item slot.</returns>
	public static string [][] GetInventories ()
	{
		string[][] items = new string[INVENTORY_SLOTS] [];
		string query = 
			"SELECT DISTINCT Inventory.slot_no, Item.item_id, Item.item_name, item_type_name, price, level, armor, " + 
			"attack, speed, amount, max_amount, quantity " + 
			"FROM Item, Item_type, Inventory " + "" +
			"WHERE Item.item_type_id = Item_type.item_type_id AND " +
			"Item.item_id = Inventory.item_id " +
			"AND Inventory.player_id = @player_id;";
		try 
		{
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{
				using (MySqlCommand cmd = new MySqlCommand (query, conn)) 
				{
					conn.Open ();
					cmd.Parameters.AddWithValue ("@player_id", playerId);
					cmd.Prepare ();

					using (MySqlDataReader reader = cmd.ExecuteReader ()) 
					{

						for (int i=0; i<INVENTORY_SLOTS; i++) 
						{
							if (reader.HasRows && reader.Read ())
							{
								items [i] = ReadItems (reader, 12);

								// synchronise inventory slot number and array index
								int slot = int.Parse (items[i][0]) - 1;
								if (slot != i)
								{
									items[slot] = items[i];
									items[i] = null;
									i=slot;
								}
							}
						}
					}
				}
			}
			return items;
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in ListInventory :\n" + ex);
			return null;
		}
	}
		
	/// <summary>
	/// Lists all the items that the player currently equips.
	/// </summary>
	/// <returns>All items that the current player equips.
	/// Double array will be returnd whose each row represents the item in each equipment item slot. </returns>
	public static string [][] GetEquipments ()
	{
		string[][] equips = new string[EQUIPMENT_SLOTS] [];
		string query = 
			"SELECT slot_no, Item.item_id, Item.item_name, item_type_name, " +
			"price, level, armor, attack, speed, amount, max_amount "+
			"FROM Item, Item_type, Equipment " + 
			"WHERE Item.item_type_id = Item_type.item_type_id " + 
			"AND Item.item_id = Equipment.item_id " + 
			"AND Equipment.player_id = @player_id;";
		try 
		{
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{
				using (MySqlCommand cmd = new MySqlCommand (query, conn)) 
				{
					conn.Open ();
					cmd.Parameters.AddWithValue ("@player_id", playerId);
					using (MySqlDataReader reader = cmd.ExecuteReader ()) 
					{
						for (int i=0; i<INVENTORY_SLOTS; i++) 
						{
							if (reader.HasRows && reader.Read ())
							{
								equips [i] = ReadItems (reader, 11);

								// synchronise inventory slot number and array index
								int slot = int.Parse (equips [i][0]) - 1;
								if (slot != i)
								{
									equips [slot] = equips [i];
									equips [i] = null;
									i=slot;
								}
							}
						}
					}
				}
			}
			return equips;
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in ListEquipments :\n" + ex);
			return null;
		}
	}

	/// <summary>
	/// Returns the player's statistics.
	/// </summary>
	/// <returns> All the elements in the Statistics table:
	// 0: num_defeat
	// 1: num_boss_dereat
	// 2: max_rounds
	// 3: num_downed
	// 4: num_revived
	// 5: num_get_revived.</returns>
	public static int [] GetStatistics ()
	{
		int[] stats = new int [STATISTICS_COLS];
		string[] stats_str = null;
		string query = 
			"SELECT num_defeat, num_boss_defeat, max_rounds, num_downed, num_revived, num_get_revived " + 
			"FROM Statistics " +
			"WHERE player_id = @player_id;";
		try 
		{
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{
				using (MySqlCommand cmd = new MySqlCommand (query, conn)) 
				{
					conn.Open ();
					cmd.Parameters.AddWithValue ("@player_id", playerId);

					using (MySqlDataReader reader = cmd.ExecuteReader ()) 
					{

						if (reader.HasRows && reader.Read ())
						{
							stats_str = ReadItems (reader, STATISTICS_COLS);
						}
					}
				}
			}
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in GetStatistics :\n" + ex);
			return null;
		}

		for  (int i=0; i<STATISTICS_COLS; i++)
			stats[i] = int.Parse (stats_str[i]);
		return stats;
	}
		
	/// <summary>
	/// Updates the specified slot of the Inventory table.
	/// If itemId is not specified or is 0, then null will be set to the slot.
	/// If itemId is specified but addition is not (or 0 is passed) quantity of the item will be added by 1.
	/// If both itemId and addition are specified, quantity of the item will be added by addition. 
	/// </summary>
	/// <returns><c>true</c>, if inventory was updated, <c>false</c> otherwise.</returns>
	/// <param name="slotNum">Slot number.</param>
	/// <param name="itemId">Item identifier.</param>
	/// <param name="addition">Number of items to be added (negative means decrease in number).</param>
	public static bool UpdateInventory (int slotNum, int itemId = 0, int addition = 0)
	{
		// Minimum input check.
		if (slotNum <= 0 || itemId < 0)
			return false;

		string updateInventory =
			"UPDATE Inventory SET item_id=@item_id, quantity=quantity+(@addition) " +
				"WHERE player_id=@player_id AND slot_no=@slot_no;";
		string postUpdateInventory =
			"UPDATE Inventory SET item_id=IF (quantity<=0, NULL, item_id), quantity=IF (quantity<=0, 0, quantity) "  +
			"WHERE player_id=@player_id AND slot_no=@slot_no;";
		string setQuantityToZero =
			"UPDATE Inventory SET quantity=0 WHERE player_id=@player_id AND slot_no=@slot_no;";

		try
		{
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{

				conn.Open ();
				MySqlTransaction trans;
				MySqlCommand cmd = conn.CreateCommand ();
				trans = conn.BeginTransaction ();
				cmd.CommandText = updateInventory;
				cmd.Connection = conn;
				cmd.Transaction = trans;
				try
				{
					cmd.Parameters.AddWithValue ("@player_id", playerId);
					if (itemId == 0)
					{
						cmd.Parameters.AddWithValue ("@item_id", null);
						cmd.Parameters.AddWithValue ("@addition", 0);
					}
					else
					{
						cmd.Parameters.AddWithValue ("@item_id", itemId);
						if (addition != 0)
							cmd.Parameters.AddWithValue ("@addition", addition);
						else
							cmd.Parameters.AddWithValue ("@addition", 1);
					}					
					cmd.Parameters.AddWithValue ("@slot_no", slotNum);
					cmd.ExecuteNonQuery ();

					// Post updating
					if (itemId > 0 && addition != 0)
					{
						// Set quantity to 0 if it went to negative and
						// set null to item_id if quantity has become 0.
						cmd.CommandText = postUpdateInventory;
						cmd.ExecuteNonQuery ();
					}
					else if (itemId <= 0)
					{
						// Set the quantity to zero.	
						cmd.CommandText = setQuantityToZero;
						cmd.ExecuteNonQuery ();
					}
					// Commits the transaction.
					trans.Commit ();
					return true;
				}
				catch (MySqlException ex) 
				{
					// Rollbacks the transaction.
					trans.Rollback ();
					Debug.LogError ("Error in UpdateInventory :\n" + ex);
					return false;
				}
			}
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in UpdateInventory :\n" + ex);
			return false;
		}
	}
		
	/// <summary>
	/// Updates the specified slot of the Equipment table.
	/// If itemId is not specified or is 0, null will be set to the slot.
	/// </summary>
	/// <returns><c>true</c>, if equipment was updated, <c>false</c> otherwise.</returns>
	/// <param name="slotNum">Slot number.</param>
	/// <param name="itemId">Item identifier.</param>
	public static bool UpdateEquipment (int slotNum, int itemId=0)
	{
		// Minimum input check.
		if (slotNum <= 0 || itemId < 0)
			return false;
		
		string updateEquipment = 
				"UPDATE Equipment SET item_id=@item_id WHERE player_id=@player_id AND slot_no=@slot_no;";
		try 
		{
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{
				conn.Open ();

				using (MySqlCommand cmd = new MySqlCommand (updateEquipment, conn))
				{
					cmd.Parameters.AddWithValue ("@player_id", playerId);
					if (itemId == 0)
						cmd.Parameters.AddWithValue ("@item_id", null);
					else
						cmd.Parameters.AddWithValue ("@item_id", itemId);
					cmd.Parameters.AddWithValue ("@slot_no", slotNum);
					cmd.ExecuteNonQuery ();
				}
			}
			return true;
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in UpdateEquipment :\n" + ex);
			return false;
		}
	}
		
	/// <summary>
	/// Updates player's experience point;
	/// </summary>
	/// <returns><c>true</c>, if experience point was updated, <c>false</c> otherwise.</returns>
	/// <param name="addition">Amount of the experience point to be added.</param>
	/// <param name="absolute">If > -1, Addidion is ignored and this value is SET to the experience point (not addition)</param>.</param>
	public static bool UpdateEXP (int addition, int absolute=-1)
	{
		string updateExp;
		if (absolute >= 0)
			updateExp = "UPDATE Player SET experience=@exp WHERE player_id=@player_id;";
		else 
			updateExp = "UPDATE Player SET experience=experience+@exp WHERE player_id=@player_id;";
		try 
		{
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{
				conn.Open ();

				using (MySqlCommand cmd = new MySqlCommand (updateExp, conn))
				{
					if (addition < 0)
						addition = 0;
					
					cmd.Parameters.AddWithValue ("@player_id", playerId);
					if (absolute > -1)
						cmd.Parameters.AddWithValue ("@exp", absolute);
					else
						cmd.Parameters.AddWithValue ("@exp", addition);
					cmd.ExecuteNonQuery ();
				}
			}
			return true;
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in UpdateEXP :\n" + ex);
			return false;
		}
	}
		
	/// <summary>
	/// Update player's statistics table. If there is no change for a parameter, 0 should be passed.
	/// </summary>
	/// <returns><c>true</c>, if statistics was updated, <c>false</c> otherwise.</returns>
	/// <param name="defeat">Number of enemies the player has defeated.</param>
	/// <param name="boss">Number of boss enemies the player has defeated.</param>
	/// <param name="round">Number of rounds the player has gone through.</param>
	/// <param name="down">Number of downs the playe has taken.</param>
	/// <param name="rev">Number of revives the player has given to other players.</param>
	/// <param name="get_rev">Number of revival given by other players.</param>
	public static bool UpdateStatistics (int defeat, int boss, int round, int down, int rev, int get_rev)
	{
		string updateStats = 
			"UPDATE Statistics SET num_defeat=@defeat, num_boss_defeat=@boss, " +
			"max_rounds=@round, num_revived=@rev, num_get_revived=@get_rev " +
			"WHERE player_id = @player_id;";

		try 
		{
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{
				conn.Open ();

				using (MySqlCommand cmd = new MySqlCommand (updateStats, conn))
				{
					cmd.Parameters.AddWithValue ("@player_id", playerId);
					cmd.Parameters.AddWithValue ("@defeat", defeat);
					cmd.Parameters.AddWithValue ("@boss", boss);
					cmd.Parameters.AddWithValue ("@round", round);
					cmd.Parameters.AddWithValue ("@down", down);
					cmd.Parameters.AddWithValue ("@rev", rev);
					cmd.Parameters.AddWithValue ("@get_rev", get_rev);
					cmd.ExecuteNonQuery ();
				}
			}
			return true;
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in UpdateStatistics :\n" + ex);
			return false;
		}
	}

	/// <summary>
	/// Used to purchase an item from store.
	/// It is assumed that the item price does not exceed what the player can afford.
	/// </summary>
	/// <returns><c>true</c>, if item was bought, <c>false</c> otherwise.</returns>
	/// <param name="itemId">Item identifier.</param>
	/// <param name="slotNum">Slot number.</param>
	public static bool BuyItem (int itemId, int slotNum)
	{
		// check if slot is empty
		// check the price

		// Minimum input check.
		if (itemId <= 0 || slotNum <= 0)
			return false;

		string updateInventory  = 
			"UPDATE Inventory SET item_id=@item_id, quantity=IF (quantity=0, 1, quantity+1) " +
				"WHERE player_id=@player_id AND slot_no=@slot_no;";
		string updatePlayer  = 
			"UPDATE Player SET experience=experience-(SELECT price FROM Item WHERE item_id=@item_id) " +
				"WHERE player_id = @player_id;";

		try
		{
			using (MySqlConnection conn = new MySqlConnection (GetConnectionString ())) 
			{
				conn.Open ();
				MySqlCommand cmd = conn.CreateCommand ();
				MySqlTransaction trans;
				trans = conn.BeginTransaction ();
				cmd.Connection = conn;
				cmd.Transaction = trans;

				try 
				{
					cmd.CommandText = updateInventory;
					cmd.Parameters.AddWithValue ("@player_id", playerId);
					cmd.Parameters.AddWithValue ("@item_id", itemId);
					cmd.Parameters.AddWithValue ("@slot_no", slotNum);
					cmd.ExecuteNonQuery ();
					cmd.Parameters.Clear ();

					cmd.CommandText = updatePlayer;
					cmd.Parameters.AddWithValue ("@player_id", playerId);
					cmd.Parameters.AddWithValue ("@item_id", itemId);
					cmd.ExecuteNonQuery ();

					// Commits the transaction.
					trans.Commit ();
					return true;
				}
				catch (MySqlException ex) 
				{
					// Rollbacks the transaction.
					trans.Rollback ();
					Debug.LogError ("Error in BuyItem :\n" + ex);
					return false;
				}
			}
		}
		catch (MySqlException ex) 
		{
			Debug.LogError ("Error in BuyItem :\n" + ex);
			return false;
		}
	}
		
	/// <summary>
	/// A helper method used to read each table column value from the MySqlDataReader object
	/// for number of times specified by numCols.
	/// </summary>
	/// <returns>Array of table items that have been read.</returns>
	/// <param name="reader">Query reader.</param>
	/// <param name="numCols">Number cols to be read.</param>
	private static string [] ReadItems (MySqlDataReader reader, int numCols)
	{
		string [] column = new string [numCols];
		for (int i = 0; i < numCols; i++)
			column [i] = reader.GetString (i);
		return column;
	}
}
