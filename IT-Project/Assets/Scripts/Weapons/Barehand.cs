﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Represents the weapon Barehand which includes attacking by body (e.g., slime).
/// </summary>
public class Barehand : Weapon 
{
	override public void Attack ()
	{
		if (CanAttack ())
		{
			anim.Play ("Attack");
			playAttackSound ();

			// for multiplay
			if (!GameManager.Instance.singleplay)
			{
				attacker.netPlayer.PlayAttackSoundRPC ();
				attacker.netPlayer.PlayAnimRPC ("Attack");
			}
				
			RaycastHit hit;
			Character opp;

			// Find one object within the range of attack
			if (Physics.SphereCast (transform.position, attackRadius, transform.TransformDirection (Vector3.forward), out hit, range) && 
				(opp = hit.transform.GetComponent<Character> ()) != null)
				applyDamage (hit);
		}
	}

    protected override float calcDamage(Character opp)
    {
        int diffference = transform.GetComponent<Enemy>().attack - opp.transform.GetComponent<Player>().armor;
        Debug.Log(diffference);
        return diffference;
        
    }
}
