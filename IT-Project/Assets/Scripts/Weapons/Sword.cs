﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Represents a sword.
/// </summary>
public class Sword : Weapon 
{

    protected override float calcDamage(Character opp)
    {
        int diffference = transform.GetComponent<Character>().attack - opp.transform.GetComponent<Enemy>().armor;
        Debug.Log(diffference);
        return diffference;
        
    }


    override public void Attack ()
	{
		if (CanAttack ())
		{
			anim.Stop (); // Prevents attacking without motion (attaking with only the attacking sound).
			// for local
			anim.Play ("Attack");
			playAttackSound ();
			// for multiplay
			if (!GameManager.Instance.singleplay)
			{
				attacker.netPlayer.PlayAttackSoundRPC ();
				attacker.netPlayer.PlayAnimRPC ("Attack");
			}

			// Find all objects within the attack range and attack radius.
			RaycastHit[] hits = Physics.SphereCastAll (transform.position, attackRadius, transform.TransformDirection(Vector3.forward), range);
			for (int i = 0; i < hits.Length; i++) 
			{
				hits [i].transform.GetComponent<Character> ();
				if ((hits [i].transform.GetComponent<Character> ()) != null)
					applyDamage (hits [i]);
			}
		}
	}
}
