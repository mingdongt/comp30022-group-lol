﻿using UnityEngine;
using System.Collections;

public abstract class Weapon : MonoBehaviour {

	public float range = 100.0f; // attack distance (not radius)
	public float damage = 200.0f;
	public float attackRadius = 20.0f;
	public float volLowRange = 0.1f;
	public float volHighRange = 1.0f;
	public float dist = 1.0f;
	public AudioClip attackSoundClip;
	public AudioClip hitSoundClip;
	protected AudioSource soundSource;
	protected Character attacker;
	protected Animation anim;

	public void Awake ()
	{
		// Gets Actor object.
		if ( ( soundSource = GetComponent<AudioSource> ()) == null)
			Debug.LogError ("The character needs to a sound source.");
		
		// Gets Actor object.
		if ( (attacker = GetComponent<Character> ()) == null)
			Debug.LogError ("The character needs to be an character.");

		// Gets Animation object.
		if ( (anim = GetComponent<Animation> ()) == null)
			Debug.LogError ("The character needs to an Animation.");
	}

	/// <summary>
	/// Returns if the parameter is object of type Player.
	/// </summary>
	/// <returns><c>true</c>, if I player was amed, <c>false</c> otherwise.</returns>
	/// <param name="me">Sender.</param>
	private bool amIPlayer (Character me)
	{
		return me.GetType () == typeof(Player);
	}

	/// <summary>
	/// Returns if the parameter is object of type Enemy.
	/// </summary>
	/// <returns><c>true</c>, if I player was amed, <c>false</c> otherwise.</returns>
	/// <param name="me">Sender.</param>
	private bool amIEnemy (Character me)
	{
		return me.GetType () == typeof(Enemy);
	}

	// Calculates the damage.
	protected virtual float calcDamage (Character opp)
	{
        return 3.0f;
	}

	/// <summary>
	/// Plays attack sound when it did not hit the opponent.
	/// </summary>
	[PunRPC]
	public void playAttackSound ()
	{
		//Debug.Log ("PlayAttackSound called");
		//
		// !!! Volume should reflect the distance from the source
		//
		soundSource.PlayOneShot (attackSoundClip, Random.Range (volLowRange, volHighRange));
	}

	/// <summary>
	/// Plays the sround when the attack actually hits the opponent.
	/// </summary>
	//public void _playHitSound (PhotonPlayer target, PhotonMessageInfo info)

	[PunRPC]
	public void playHitSound ()
	{
		//Debug.Log ("PlayHitSound called");
		//
		// !!! Volume should reflect the distance from the source
		//
		soundSource.PlayOneShot (hitSoundClip, Random.Range (volLowRange, volHighRange));
	}

	/// <summary>
	/// Checks if the last attack was performed within the specified time interval.
	/// </summary>
	/// <returns><c>true</c>, if the last attack was outside the interval, <c>false</c> otherwise.</returns>
	public bool CanAttack ()
	{
		return attacker.CanAttack ();
	}

	/// <summary>
	/// Applies damage to the one who got attackes.
	/// </summary>
	/// <param name="hit">player or enemy who got attacked.</param>
	protected void applyDamage (RaycastHit hit)
	{
		Character opp = hit.transform.GetComponent<Character> ();

		if (opp != null && attacker != opp)
		{
			if ((amIPlayer (attacker) && amIEnemy (opp)) ||	(amIPlayer (opp) && amIEnemy (attacker) ))
			{
				dist = Vector3.Distance (opp.transform.position, attacker.transform.position);

				// If the opponent is within the specified distance, the attack is effective.
				if (dist <= range) 
				{
					int photonId;

					// local update
					playHitSound ();
					opp.TakeDamage (calcDamage (opp));

					// Sending RPC if the attacker is player
					if (!GameManager.Instance.singleplay) 
					{
						// Find a hit target and send RPC for sync of sound and damage
						photonId = hit.rigidbody.gameObject.transform.root.GetComponent <PhotonView> ().viewID;
						attacker.netPlayer.PlayHitSoundRPC ();
						attacker.netPlayer.TakeDamageRPC (photonId, calcDamage (opp));
					}
				}
			}
		}
	}

	public abstract void Attack ();

}
