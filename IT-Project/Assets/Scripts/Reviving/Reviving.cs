﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class is resposible for defining logic related
/// to the revive feature. The revive feature allows players
/// to revive 'downed' players and bring them back to life
/// over the network. This feature is only available in multiplayer.
/// </summary>
public class Reviving : MonoBehaviour {

	public Texture2D reviveButton;
	public int reviveCount;
	public float reviveRadius;

	bool revived;
	int  counter;
	bool isButtonPressed;
	bool gettingRevived;
	bool withinReviveRadius;

	public bool GettingRevived {

		get {
			return gettingRevived;
		}
		set {
			gettingRevived = value;
		}
	}


	public bool Revived {

		get {
			return revived;
		}
		set {
			revived = value;
		}
	}

	public bool WithinReviveRaduis {

		get {
			return withinReviveRadius;
		}
		set {
			withinReviveRadius = value;
		}
	}



	// Use this for initialization
	void Start () {

		revived = false;
		isButtonPressed = false;
		gettingRevived = false;
		withinReviveRadius = false;
		counter = 0;
	}

	// Update is called once per frame	
	void Update () {

		if (isButtonPressed) {

			counter += 1;
			GetComponent<NetworkPlayer> ().GettingRevivedRPC (GetComponent<PhotonView>().viewID,true);

			if (counter == reviveCount) {
				revived = true;
				GetComponent<NetworkPlayer> ().RevivedRPC (GetComponent<PhotonView>().viewID);
				GetComponent<NetworkPlayer> ().ResetReviveRPC (GetComponent<PhotonView> ().viewID);
			}
		} else {
			counter = 0;
			GetComponent<NetworkPlayer> ().GettingRevivedRPC (GetComponent<PhotonView>().viewID,false);

		}


	}

	/// <summary>
	/// Raises the GUI event.
	/// </summary>
	void OnGUI() {

		Transform playerPosition = GetComponent<Transform> ();
		Vector3 screenPosition = Camera.main.WorldToScreenPoint (playerPosition.position);
		screenPosition.y = Screen.height - screenPosition.y;

		withinReviveRadius = CheckWithinReviveRadius ();

		if (!revived && !gettingRevived && withinReviveRadius) {

			if (GUI.RepeatButton (new Rect (screenPosition.x - 30, screenPosition.y - 120, 75, 40), reviveButton)) {

				isButtonPressed = true;
			} else {
				isButtonPressed = false;
			}
		}
	}

	/// <summary>
	/// Checks if any players are within the a certain radius
	/// of the downed player. If they are within that certain
	/// radius they alive players able to see the revive option
	/// on top of the downed players head. If they are not within
	/// the certain radius the revive button is not visible for that
	/// player.
	/// </summary>
	/// <returns><c>true</c>, if within revive radius was checked, <c>false</c> otherwise.</returns>
	public bool CheckWithinReviveRadius() {

		GameObject[] players = GameObject.FindGameObjectsWithTag ("Player");

		for (int i = 0; i < players.Length; i++) {

			if (players [i] != this.gameObject) {

				float distance = Vector3.Distance (players [i].GetComponent<Transform> ().position,
					this.GetComponent<Transform> ().position);

				if (players [i].GetPhotonView ().isMine && !players[i].GetComponent<NetworkPlayer>().IsDown) {

					if (distance < reviveRadius)
						return true;
					else
						return false;
				}
			}
		}
		return false;
	}



}