﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Scenes manager. Responsible for handling the events when scenes are loaded.
/// This class is also responsible for initialising the scene.
/// </summary>
public class ScenesManager : MonoBehaviour 
{ 
	private static ScenesManager instance;
	public ScenesManager Instance { get { return instance; } }

	void Awake () 
	{
		if (instance == null) 
		{
			instance = this;
		} else 
		{
			Destroy (gameObject);
		}
	}
		
	/// <summary>
	/// This function is obsolete for Unity Ver 5.4 and above
	/// Handles initialisation processes of each scene loaded.
	/// </summary>
	void OnLevelWasLoaded ()
	{
        Scene thisScene = SceneManager.GetActiveScene();
        string sceneName = thisScene.name;

		if (sceneName == "GameMain") 
		{
			if (GameManager.Instance.singleplay) 
			{
				// Prepare for the single play.
				// Avoid duplicate instantiation.
				if (GameObject.Find (GameManager.Instance.singlePlayerPrefabName + "(Clone)") == null)
					Instantiate (Resources.Load (GameManager.Instance.singlePlayerPrefabName), Vector3.one, Quaternion.identity);
				GameObject enemyManager = GameObject.Find ("EnemyManager");
				enemyManager.GetComponent<EnemyManager> ().enabled = true;
				GUIManager.CloseLobby ();
			} 
			else 
			{
				// Show lobby.
				SwitchControllers (false);
				GUIManager.ShowLobby ();
			}
        }

        if(GameManager.Instance.isContinue) {
            GameManager.Instance.isContinue = false;
            GameObject.Find("SaveLoadHost").GetComponent<SaveLoad>().LoadSaveState();
        }
	}
		
	/// <summary>
	/// Initialises the scene components.
	/// </summary>
	/// <param name="player">The current player.</param>
	public void InitScene (Player player)
	{
		// Camera and joystick
		GameObject joystick = GameObject.Find ("JoystickBackgroundImage");
		VirtualJoystick virtualJoystick = joystick.GetComponent<VirtualJoystick> ();
		player.GetComponent<JoystickController> ().joystick = virtualJoystick;
		GameObject camera = GameObject.Find ("Main Camera");
		camera.GetComponent<CameraController> ().target = player.GetComponent<Transform> ().transform;

		// Health bar
		player.healthSlider = GameObject.Find ("Slider").GetComponent <Slider> ();
		player.damageImage = GameObject.Find ("DamageImage").GetComponent <Image> ();

		// Buttons
		AttackButton attackButton = GameObject.Find ("AttackButton").GetComponent <AttackButton> ();
		attackButton.player = player;
		SkillButton1 skillButton = GameObject.Find ("SkillBoomButton").GetComponent <SkillButton1> ();
		PotionButton potionButton = GameObject.Find ("PotionButton").GetComponent <PotionButton> ();
		skillButton.player = player;
		skillButton.skill = player.GetComponent <SkillBoom> ();
		potionButton.player = player;
		potionButton.potion = player.GetComponent <UsePotion> ();
	}

	/// <summary>
	/// Loads single player scenes corresponding to the level.
	/// </summary>
	/// <param name="level">Level parameter.</param>
	public void LoadSinglePlayer (int level)
	{
		GameManager.Instance.singleplay = true;
		SceneManager.LoadScene ("GameMain");
	}

	/// <summary>
	/// Loads multi player scenes corresponding to the level.
	/// </summary>
	/// <param name="level">Level parameter.</param>
	public void LoadMultiplayer (int level)
	{
		SceneManager.LoadScene ("GameMain");
	}

    /// <summary>
    /// Loads inventory scene
    /// </summary>
    public void LoadInventory()
    {
        SceneManager.LoadScene("Inventory");
    }

    /// <summary>
    /// Loads inventory scene
    /// </summary>
    public void LoadStore()
    {
        SceneManager.LoadScene("Store");
    }

    /// <summary>
    /// Turn on/off the controller UI components.
    /// </summary>
    /// <param name="flag">Represents on/off.</param>
    private void SwitchControllers (bool flag)
	{
		GameObject uiObj = GameObject.Find ("Controllers");
		Canvas ui = uiObj.GetComponent <Canvas> ();

		UnityEngine.UI.Image[] objs = ui.GetComponentsInChildren <UnityEngine.UI.Image> ();
		UnityEngine.UI.RawImage[] rawobjs = ui.GetComponentsInChildren <UnityEngine.UI.RawImage> ();

		foreach (UnityEngine.UI.Image obj in objs)
			obj.enabled = flag;

		foreach (UnityEngine.UI.RawImage obj in rawobjs)
			obj.enabled = flag;
	}

	/// <summary>
	/// Starts the multiplay scene. 
	/// Used to turn on the controller UIs for multiplayer scene.
	/// </summary>
	public void StartMultiplayScene ()
	{
		SwitchControllers (true);
	}
}
