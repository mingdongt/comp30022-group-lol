﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEditor;

public class SaveLoad : MonoBehaviour{

    public string playerPrefabName = "Cha_Knight_Single";

    private Character player;
    public GameObject instanEnemy;
    public EnemyManager enemyManager;

    public void SaveSaveState() {
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/testSave.svg");
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
		SaveState data = new SaveState ();
        data.listOfEnemies = new List<EnemyState>();

		data.playerHealth = player.healthPoint;
        data.playerDown = player.IsDown();

		data.locationX = player.transform.position.x;
		data.locationY = player.transform.position.y;
		data.locationZ = player.transform.position.z;

        Vector3 tmpPlayerV3 = player.transform.rotation.eulerAngles;
        data.rotationX = tmpPlayerV3.x;
        data.rotationY = tmpPlayerV3.y;
        data.rotationZ = tmpPlayerV3.z;

        data.enemyLevel = enemyManager.enemiesLevel;
        data.enemiesDefeated = enemyManager.enemiesDefeated;
        data.enemiesSpawned = enemyManager.enemiesSpawned;
        data.isNewWave = enemyManager.newWave;
        //v.1
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");

        foreach (GameObject go in enemies) {
            EnemyState temp = new EnemyState();
            temp.locationX = go.transform.position.x;
            temp.locationY = go.transform.position.y;
            temp.locationZ = go.transform.position.z;

            Vector3 tmpGoV3 = go.transform.rotation.eulerAngles;
            temp.rotationX = tmpGoV3.x;
            temp.rotationY = tmpGoV3.y;
            temp.rotationZ = tmpGoV3.z;

            Character aChar = go.GetComponent<Character>();
            temp.hP = aChar.healthPoint;

            Enemy anEnemy = go.GetComponent<Enemy>();
            temp.level = anEnemy.level;

            data.listOfEnemies.Add(temp);
        }

        //v.1
		bf.Serialize (file, data);
		file.Close ();
	}

	public void LoadSaveState() {
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Open(Application.persistentDataPath + "/testSave.svg",FileMode.Open);
		SaveState data = (SaveState)bf.Deserialize (file);
		file.Close ();


        //v.1
        //add code to set healthslider
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();


        Vector3 tmpPlayerV3 = new Vector3 (data.locationX, data.locationY, data.locationZ);
        Vector3 tmpPlayerRotate = new Vector3(data.rotationX, data.rotationY, data.rotationZ);
        Quaternion tmpPlayerQ = Quaternion.Euler(tmpPlayerRotate);
        player.transform.rotation= tmpPlayerQ;
        player.transform.position = tmpPlayerV3;

		player.healthPoint = data.playerHealth;
        Player tmpPlayer = player.GetComponent<Player>();
        tmpPlayer.healthSlider.value = data.playerHealth;
        if(data.playerDown) {
            player.TakeDown();
        } else {
            if(player.IsDown())
                player.BringUp();
        }

        enemyManager.enemiesLevel = data.enemyLevel;
        enemyManager.enemiesSpawned = data.enemiesSpawned;
        enemyManager.enemiesDefeated = data.enemiesDefeated;
        enemyManager.newWave = data.isNewWave;
        //v.1
        GameObject[] gos = GameObject.FindGameObjectsWithTag("enemy");
        foreach (GameObject go in gos)
            Destroy(go);

        foreach (EnemyState es in data.listOfEnemies) {
            Vector3 tmpV3 = new Vector3(es.locationX, es.locationY, es.locationZ);
            Vector3 tmpEulerV3 = new Vector3(es.rotationX, es.rotationY, es.rotationZ);
            Quaternion tmpQ = Quaternion.Euler(tmpEulerV3);
            GameObject tmpEnemy = Instantiate(instanEnemy, tmpV3, tmpQ) as GameObject;
            Character tmpEnemy2 = tmpEnemy.GetComponent<Character>();
            tmpEnemy2.healthPoint = es.hP;
            Enemy anEnemy = tmpEnemy.GetComponent<Enemy>();
            anEnemy.level = es.level;

        }


    }

    /*
	void OnGUI() {
		if (GUI.Button (new Rect (10, Screen.height/4, Screen.width/10, Screen.height/10), "Save")) {
			SaveSaveState ();
		}
		if (GUI.Button (new Rect (10, Screen.height/4 + Screen.height/10, Screen.width/10, Screen.height/10), "Load")) {
			LoadSaveState ();
		}
	}
    */

}

[System.Serializable]
class SaveState {
	//cant serialize vector3, so save 3 floats
	public float locationX;
	public float locationY;
	public float locationZ;
    //same for euler vector3
    public float rotationX;
    public float rotationY;
    public float rotationZ;

	public float playerHealth;
    public bool playerDown;
    public List<EnemyState> listOfEnemies;

    public int enemyLevel;
    public int enemiesSpawned;
    public int enemiesDefeated;
    public bool isNewWave;
}
[System.Serializable]
class EnemyState {
    public float locationX;
    public float locationY;
    public float locationZ;
    public float rotationX;
    public float rotationY;
    public float rotationZ;
    public float hP;
    public int level;
}