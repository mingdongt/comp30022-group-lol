﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// The GameManager is responsible for setting configurations
/// for a certain type of gamemode. SinglePlayer is different to
/// Multiplayer, this class's main job is to distinguish the two via
/// enabling and disabling certain components
/// </summary>
public class GameManager : MonoBehaviour 
{
	[HideInInspector]
	public bool singleplay;
    [HideInInspector]
    public bool isContinue;
    [HideInInspector]
	private bool isLoggedIn;

	private static GameManager instance;
	private NetworkManager networkManager;
	private ScenesManager sceneManager;

	public string singlePlayerPrefabName = "Cha_Knight_Single";
	public string multiPlayerPrefabName = "Cha_Knight_Multi";
	public string singleEnemyPrefabName = "Cha_Slime_Single";
	public string multiEnemyPrefabName = "Cha_Slime_Multi";

	public static GameManager Instance { get { return instance; } }
	public NetworkManager NetManager { get { return networkManager; } }
	public ScenesManager ScManager { get { return sceneManager; } }

	void Awake () 
	{
		// Applying singleton
		if (instance == null) 
		{
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else 
		{
			Destroy (gameObject);
		}

		singleplay = false;
		isLoggedIn = false;
	}

	void Start ()
	{
		networkManager = GetComponentInChildren <NetworkManager> ();
		sceneManager = GetComponentInChildren <ScenesManager> ();
	}

	/// <summary>
	/// Restarts the game.
	/// </summary>
	public void Restart ()
	{
		if (!singleplay)
			GameManager.Instance.NetManager.Reconnect ();
		SceneManager.LoadScene("sc_Title");
	}
		
	/// <summary>
	/// Starts the single play. Used to start singleplayer mode.
	/// </summary>
	public void StartSinglePlay ()
	{
		singleplay = true;
		sceneManager.LoadSinglePlayer (1);
	}

    /// <summary>
    /// Starts the inventory of player.
    /// </summary>
    public void StartInventory()
    {
        sceneManager.LoadInventory();
    }

    /// <summary>
    /// Starts the store for player.
    /// </summary>
    public void StartStore()
    {
        sceneManager.LoadStore();
    }

    /// <summary>
    /// Used to make an attempt to login into account.
    /// </summary>
    /// <param name="playerName">Pser name.</param>
    /// <param name="password">Password.</param>
    public static bool Login (string playerName, string password)
	{
		return DBManager.Login (playerName, password);
	}
		
	/// <summary>
	/// Sets or returns the status of database availablity.
	/// </summary>
	/// <value><c>true</c> if the player is logged in; otherwise, <c>false</c>.</value>
	public bool IsLoggedIn
	{
		get	{ return isLoggedIn; }
		set { isLoggedIn = value; }
	}
		
	/// <summary>
	/// Sets the player name.
	/// </summary>
	/// <param name="name">Player name.</param>
	public void SetPlayerName (string name)
	{
		NetManager.SetPlayerName (name);
	}
		
	/// <summary>
	/// Returns the name of the player.
	/// </summary>
	/// <returns>The player name.</returns>
	public string GetPlayerName ()
	{
		return NetManager.GetPlayerName ();
	}
		
	/// <summary>
	/// Used to start multiplayer mode.
	/// </summary>
	public void StartMultiPlay ()
	{
		singleplay = false;
		networkManager.enabled = true;
		sceneManager.LoadMultiplayer (1);
	}

	void Update () 
	{
		// update	
	}
}
