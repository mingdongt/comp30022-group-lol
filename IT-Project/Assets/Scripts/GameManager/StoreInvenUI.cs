﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StoreInvenUI : MonoBehaviour {

    Rect bgRect;
    Rect exitRect;

    public Texture bgImage;
    public Texture panelImage;

    // Use this for initialization
    void Start () {

        bgRect = new Rect(0, 0, Screen.width, Screen.height);
        exitRect = new Rect(Screen.width * 0.05f, Screen.height * 0.85f, Screen.width * 0.1f, Screen.height * 0.1f);
    }

    void OnGUI () {
        GUI.DrawTexture(bgRect, bgImage, ScaleMode.StretchToFill);

        GUIStyle buttonStyle = new GUIStyle(GUI.skin.button);
        buttonStyle.fontSize = Screen.width/40;

        GUI.DrawTexture(exitRect, panelImage);
        if(GUI.Button(exitRect, "Return", buttonStyle)) {
            SceneManager.LoadScene("sc_Title");
        }
    }
}
