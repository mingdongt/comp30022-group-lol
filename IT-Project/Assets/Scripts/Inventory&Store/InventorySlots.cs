﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

namespace inv{
    /// <summary>
    /// InventorySlots.
    /// 1. Display inventory changes for player and handle inventory data changes for database.
    /// 2. Integrated and cooperate with store and character slots to display complete inventory and store system.
    /// </summary>
    public class InventorySlots : MonoBehaviour
    {

        public struct item
        {
            public string type;
            public int level;

            //The number of following attributes would be generated depending on the level in the store, except maxAmount.
            public int armor;
            public int attack;
            public int speed;
            public int price;
            public int amount;
            public int maxAmount;
        }

        public Texture2D[] icons;
        public item[] content = new item[12];
        public bool testMode;
        private string[][] itemData;
        private GUIStyle info= new GUIStyle();

        item checkedItem = new item();
        bool checking = false;

        Vector2 windowSize = new Vector2(Screen.width*3/4, Screen.height*2/3);
        Vector2 itemIconSize = new Vector2(Screen.width * 3 / 32, Screen.width * 3 / 32);
        Vector2 offSet = new Vector2(Screen.width * 3 / 384, Screen.width * 3 / 160);
        Rect windowRect = new Rect();
        bool inventoryMode = false;
        bool storeMode = false;

        CharacterSlots character;
        Store store;

        
        // Use this for initialization
        void Start()
        {
            info.fontSize = Screen.width/32;
            info.normal.textColor = Color.white;
            if (testMode)
            {
                DBManager.Login("player2", "pass"); 
            }

            // fetch item catalogue
            itemData = DBManager.GetItems();
            checkedItem.type = "";
            windowRect = new Rect(Screen.width - windowSize.x, Screen.height - windowSize.y, windowSize.x, windowSize.y);

            if (character = transform.GetComponent<CharacterSlots>())
            {
                inventoryMode = true;
            }
            else if (store = transform.GetComponent<Store>())
            {
                storeMode = true;
            }
            else
            {
                Debug.LogError("The inventory doesn't have another dependency to cooperate with");
            }

            displayRemoteInventory();
        }

        /// <summary>
        /// check if there is enough space in the inventory for a particular item
        /// </summary>
        /// <param name="item">an instance of item struct.</param>
        public bool hasSpace(item item)
        {
            for (int x = 0; x < content.Length; x++)
            {
                if (content[x].amount == 0)
                {
                    return true;
                }
                if (content[x].type == item.type)
                {
                    if (content[x].maxAmount > item.amount + content[x].amount)
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        /// <summary>
        /// check if there is enough money in the invenotry for buying a particular item
        /// </summary>
        /// <param name="item">an instance of item struct.</param>
        public bool hasEnoughMoney(item item)
        {
            int sum = 0;
            for (int x = 0; x < content.Length; x++)
            {
                if (content[x].type == "Gold")
                {
                    sum += content[x].amount;
                }
            }
            if (sum >= item.price)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// handle removing gold whose amount is equal to the price of item from inventory 
        /// </summary>
        /// <param name="item">an instance of item struct.</param>
        public void payStore(item item)
        {
            int need = item.price;
            for (int x = 0; x < content.Length; x++)
            {
                if (content[x].type == "Gold")
                {
                    if (content[x].amount > need)
                    {
                        content[x].amount -= need;
                        need = 0;
                        DBManager.UpdateInventory(x + 1);
                        DBManager.UpdateInventory(x + 1, findItemID(content[x]), content[x].amount);
                    }
                    else
                    {
                        content[x].amount = 0;
                        need -= content[x].amount;
                        content[x] = new item();
                        DBManager.UpdateInventory(x + 1);
                    }
                }
                if (need == 0)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// return the index of the first empty slot in the inventory.
        /// </summary>
        int findEmptyCell()
        {
            for (int x = 0; x < content.Length; x++)
            {
                if (content[x].amount == 0)
                {
                    return x;
                }
            }
            return -1;
        }

        /// <summary>
        /// handle adding an item to the inventory
        /// </summary>
        /// <param name="i">an instance of item struct.</param>
        public void addItem(item i)   
        {
            bool found = false;

            // equipment must be put seperately
            if (i.maxAmount == 1)
            {
                DBManager.UpdateInventory(findEmptyCell() + 1, findItemID(i), 1);
                content[findEmptyCell()] = i;
                    
            }

            //potions and gold
            else
            {
                for (int x = 0; x < content.Length; x++)
                {
                    //stack if possible
                    if (i.type == content[x].type)
                    {
                        if (content[x].amount + i.amount <= content[x].maxAmount)
                        {
                            content[x].amount += i.amount;
                            DBManager.UpdateInventory(x + 1, findItemID(i), i.amount);
                            found = true;
                        }
                    }
                }
                if (!found)
                {
                    DBManager.UpdateInventory(findEmptyCell() + 1, findItemID(i), i.amount);
                    content[findEmptyCell()] = i;
                    
                }

            }
        }

        /// <summary>
        /// handle removing an item from inventory
        /// </summary>
        /// <param name="i">an instance of item struct.</param>
        public void removeItem(item i)
        {
            bool found = false;
            for (int x = 0;x < content.Length;x ++)
            {
                if (content[x].Equals(i))
                {
                    if (i.amount > 1)
                    {
                        content[x].amount -= 1;
                    }
                    else
                    {
                        content[x] = new item();
                    }
                    DBManager.UpdateInventory(x + 1, findItemID(i), -1);
                    found = true;
                }
                if (found)
                {
                    break;
                }
            }
        }


        /// <summary>
        /// Manipulate selling an item to the store
        /// </summary>
        /// <param name="item">an instance of item struct.</param>
        public void sellItem(item item)
        {
            addItem(goldOf(item));
            removeItem(item);
        }


        /// <summary>
        /// Manipulate buying an item from the store
        /// </summary>
        /// <param name="item">an instance of item struct.</param>
        public void buyItem(item item)
        {
            addItem(item);
            payStore(item);
        }

        /// <summary>
        /// return gold whose amount is equal to the the price of given item sold to store 
        /// </summary>
        /// <param name="item">an instance of item struct.</param>
        item goldOf(item item)
        {
            float depreciation = 0.5f;
            item gold = new item();

            gold.type = "Gold";
            gold.maxAmount = 10000;
            gold.amount = (int) (item.price* depreciation);
            return gold;
        }


        /// <summary>
        /// get and copy remote inventory data to the local for current player, called upon the inventory is opened.
        /// </summary>
        public void displayRemoteInventory()
        {

            string[][] remote = DBManager.GetInventories();
         
            for (int i = 0;i < content.Length; i ++)
            {
                item temp = new item();
                if (remote[i] != null)
                {
                    temp.type = remote[i][3];
                    temp.price = Int32.Parse(remote[i][4]);
                    temp.level = Int32.Parse(remote[i][5]);
                    temp.armor = Int32.Parse(remote[i][6]);
                    temp.attack = Int32.Parse(remote[i][7]);
                    temp.speed = Int32.Parse(remote[i][8]);
                    temp.maxAmount = Int32.Parse(remote[i][10]);
                    temp.amount = Int32.Parse(remote[i][11]);
                }
                content[i] = temp;
            }
        }

        /// <summary>
        /// find ID of given item in the item catalogue fetched from database.
        /// </summary>
        /// <param name="item">an instance of item struct.</param>
        public int findItemID(item item)
        {
            foreach (string[] record in itemData)
            {
                if (record[2] == item.type)
                {
                    if (item.type == "Potion" || item.type == "Reviving" || item.type == "Gold")
                    {
                        return Int32.Parse(record[0]);
                    }
                    else
                    {
                        if (Int32.Parse(record[4]) == item.level)
                        {
                            
                            return Int32.Parse(record[0]);
                        }
                    }
                }
            }
            return 0;
        }

        void OnGUI()
        {
            Scene scene = SceneManager.GetActiveScene();
            string name = scene.name;
            if (name == "Inventory" || name == "Store")
                windowRect = GUI.Window(0, windowRect, displayInventoryWindow, "Inventory");
        }

        void displayInventoryWindow(int windowID)
        {
            float currentX = 0 + offSet.x;
            float currentY = 18 + offSet.y;
            int index = 0;
            bool exist;

            // check button
            if (checking)
            {
                if (GUI.Button(new Rect(windowSize.x - itemIconSize.x * 2, 0, itemIconSize.x * 2, itemIconSize.y / 2-5), "Checking...."))
                {
                    checking = false;
                }
            }
            else
            {
                if (GUI.Button(new Rect(windowSize.x - itemIconSize.x * 2, 0, itemIconSize.x * 2, itemIconSize.y / 2-5), "Check Info"))
                {
                    checking = true;
                }
            }
            
            // slot buttons
            foreach (item j in content)
            {
                exist = false;

                foreach (Texture2D t in icons)
                {
                    // something in the cell
                    if (t.name == j.type)
                    {
                        // render the icon for it
                        if (GUI.Button(new Rect(currentX, currentY, itemIconSize.x, itemIconSize.y), t))
                        {

                            if (checking) 
                            {
                                checkedItem = j;
                                
                            }
                            else
                            {
                                // equip item which is on the button pushed by player in inventory mode.
                                if (inventoryMode)
                                {
                                    if (j.type != "Gold" && j.type != "Potion" && j.type != "Reviving")
                                    {
                                        character.equipItem(j);
                                    }
                                }
                                else
                                {
                                    // sell item which is on the button pushed by player in store mode.
                                    if (hasSpace(goldOf(j)) && j.type != "Gold")
                                    {
                                        sellItem(j);
                                    } 
                                }
                                
                            }
                        }
                        exist = true;
                    }
                }

                //nothing in the cell
                if (!exist)
                {
                    if (GUI.Button(new Rect(currentX, currentY, itemIconSize.x, itemIconSize.y), ""))
                    {
                  
                    }
                }
                index++;
                currentX += itemIconSize.x + Screen.width * 3 / 384;
                if (index ==4 || index == 8)
                {
                    currentX -= 4 * itemIconSize.x + Screen.width * 3 / 96;
                    currentY += itemIconSize.y + Screen.width * 3 / 384;
                }
            }

            //display info checked
            if (checking)
            {
                
                GUI.Label(new Rect(currentX + Screen.width*3/384, Screen.width /25, Screen.width/7, Screen.width/64), "Type: " + checkedItem.type.ToString(),info);
                GUI.Label(new Rect(currentX + Screen.width * 3 / 384, Screen.width * 2 / 25, Screen.width / 7, Screen.width / 64), "Level: " + checkedItem.level.ToString(), info);
                GUI.Label(new Rect(currentX + Screen.width * 3 / 384, Screen.width * 3 / 25, Screen.width / 7, Screen.width / 64), "Armor: " + checkedItem.armor.ToString(), info);
                GUI.Label(new Rect(currentX + Screen.width * 3 / 384, Screen.width * 4 / 25, Screen.width / 7, Screen.width / 64), "Attack: " + checkedItem.attack.ToString(), info);
                GUI.Label(new Rect(currentX + Screen.width * 3 / 384, Screen.width * 5 / 25, Screen.width / 7, Screen.width / 64), "Speed: " + checkedItem.speed.ToString(), info);
                GUI.Label(new Rect(currentX + Screen.width * 3 / 384, Screen.width * 6 / 25, Screen.width / 7, Screen.width / 64), "Amount: " + checkedItem.amount.ToString(), info);
                GUI.Label(new Rect(currentX + Screen.width * 3 / 384, Screen.width * 7 / 25, Screen.width / 7, Screen.width / 64), "MaxAmount: " + checkedItem.maxAmount.ToString(), info);
                GUI.Label(new Rect(currentX + Screen.width * 3 / 384, Screen.width * 8 / 25, Screen.width / 7, Screen.width / 64), "Price: " + checkedItem.price.ToString(), info);

            }

        }
            
    }
}

