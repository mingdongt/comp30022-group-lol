﻿using UnityEngine;
using System.Collections;
using inv;
using System;
using UnityEngine.SceneManagement;
/// <summary>
/// CharacterSlots.
/// 1. Display equipement changes for player and handle equipement data changes for database.
/// 2. Integrated and cooperate with inventory slots to display complete inventory system.
/// 3. Show character personal information about attributes.
/// </summary>
public class CharacterSlots : MonoBehaviour {

    public string[] slotName;
    private Rect[] buttonPositions= new Rect [5];
    public inv.InventorySlots.item[] slot = new inv.InventorySlots.item[5];
    public Texture2D[] icons;
    public bool testMode;
    private GUIStyle info = new GUIStyle();


    Vector2 windowSize = new Vector2(Screen.width, Screen.height * 1 / 3);
    InventorySlots playerInv;
    Rect windowRect = new Rect();

    //personal information part
    public int armor;
    public int attack;
    public int speed;
    public int level;
    Vector2 itemIconSize = new Vector2(Screen.width * 3 / 32, Screen.width * 3 / 32);
    Vector2 offSet = new Vector2(Screen.width * 3 / 384, Screen.width * 3 / 160);

    // Use this for initialization
    void Start () {
        info.fontSize = Screen.width / 32;
        info.normal.textColor = Color.white;

        buttonPositions[0]= new Rect(offSet.x, 0, itemIconSize.x, itemIconSize.y);
        buttonPositions[1] = new Rect(offSet.x + itemIconSize.x, 0, itemIconSize.x, itemIconSize.y);
        buttonPositions[2] = new Rect(offSet.x, itemIconSize.y, itemIconSize.x, itemIconSize.y);
        buttonPositions[3] = new Rect(offSet.x + itemIconSize.x, itemIconSize.y, itemIconSize.x, itemIconSize.y);
        buttonPositions[4] = new Rect(offSet.x + itemIconSize.x*2, 0, itemIconSize.x, itemIconSize.y);

        if (testMode)
        {
            DBManager.Login("player2", "pass");
        }

        playerInv = transform.GetComponent<InventorySlots>();

        if (playerInv == null)
        {
            Debug.LogError("Character slots don't coppespond to an inventory");
        }

        windowRect = new Rect(0, 0, windowSize.x, windowSize.y);
        displayRemoteCha();     
    }

    /// <summary>
    /// get and copy remote equipment data to the local for current player, called upon the character slots are opened.
    /// </summary>
    public void displayRemoteCha()
    {
        string[][] remote = DBManager.GetEquipments();
        level = (int)Mathf.Sqrt(DBManager.GetPlayerEXP());
        for (int i = 0; i < slot.Length; i++)
        {
            inv.InventorySlots.item temp = new inv.InventorySlots.item();
            if (remote[i] != null)
            {
                
                temp.type = remote[i][3];
                temp.price = Int32.Parse(remote[i][4]);
                temp.level = Int32.Parse(remote[i][5]);
                temp.armor = Int32.Parse(remote[i][6]);
                temp.attack = Int32.Parse(remote[i][7]);
                temp.speed = Int32.Parse(remote[i][8]);
                temp.amount = Int32.Parse(remote[i][9]);
                temp.maxAmount = Int32.Parse(remote[i][10]);
            }
            slot[i] = temp;
        }
    }

    /// <summary>
    /// return true if the slot is tempty, false for occupied.
    /// </summary>
    /// <param name="toCheck">index of slot which is to be checked</param>
    bool checkSlot(int toCheck)
    {
        
        bool toReturn = false;
        if (slot[toCheck].amount != 0)
        {
            toReturn = true;
        }
        return toReturn;
    }

    /// <summary>
    /// equip the item in the inventory, gets called in inventory slots
    /// remove the item from the inventory
    /// </summary>
    /// <param name="i">an instance of item struct</param>
    public void equipItem(inv.InventorySlots.item i)
    {
        int index = 0; //Keeping track of where we are in the list.
        int equipto = 0; //Keeping track of where we want to be.
        foreach (string name in slotName)
        {
            if (name == i.type)
            {
                equipto = index;
            }
            index++;
        }
        if (checkSlot(equipto))
        {
            if (playerInv.hasSpace(slot[equipto]))
            {

                unequipItem(slot[equipto], equipto + 1);
                slot[equipto] = new InventorySlots.item();
            }
        }
        slot[equipto] = i;
        DBManager.UpdateEquipment(equipto + 1, playerInv.findItemID(i));
        playerInv.removeItem(i);
    }

    /// <summary>
    /// unequip the item in the inventory
    /// add the item from the inventory
    /// </summary>
    /// <param name="i">an instance of item struct</param>
    public void unequipItem(inv.InventorySlots.item i, int j)
    {
        playerInv.addItem(i);
        DBManager.UpdateEquipment(j);
    }

    void OnGUI()
    {
        Scene scene = SceneManager.GetActiveScene();
        string name = scene.name;
        if (name == "Inventory")
            windowRect = GUI.Window(1, windowRect, displayChaWindow, "Character");
    }

    public void displayChaWindow(int windowID)
    {
        int index = 0;

        foreach (InventorySlots.item item in slot)
        {
            if (item.amount == 0)
            {

                if (GUI.Button(buttonPositions[index], slotName[index]))
                {
                }

            }
            else
            {
                foreach (Texture2D t in icons)
                {
                    
                    if (t.name == item.type)
                    {
                        if (GUI.Button(buttonPositions[index], t))
                        {
                            if (playerInv.hasSpace(item))
                            {
                                unequipItem(slot[index], index+1);
                                slot[index] = new InventorySlots.item();
                                
                            }
                            
                        }

                    }
                }

            }
            index++;
        }
        calculateAttributes();
        GUI.Label(new Rect(offSet.x + itemIconSize.x*3, Screen.width / 26, Screen.width/13, Screen.width / 13), "Level: "+ level.ToString(),info);
        GUI.Label(new Rect(offSet.x + itemIconSize.x * 3, Screen.width / 13, Screen.width / 13, Screen.width / 13), "Armor: " + armor.ToString(),info);
        GUI.Label(new Rect(offSet.x + itemIconSize.x * 5, Screen.width / 26, Screen.width / 13, Screen.width / 13), "Attack: " + attack.ToString(),info);
        GUI.Label(new Rect(offSet.x + itemIconSize.x * 5, Screen.width / 13, Screen.width / 13, Screen.width / 13), "Speed: " + speed.ToString(),info);
    }


    public void calculateAttributes()
    {
        // growth 
        
        armor = 2 * level;
        attack = 2 * level + 30;
        speed = 2 * level;

        // equipment bonues
        foreach (InventorySlots.item item in slot)
        {
            armor += item.armor;
            speed += item.speed;
            attack += item.attack;
        }
        
    }
}
