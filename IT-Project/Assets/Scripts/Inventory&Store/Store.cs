﻿using UnityEngine;
using System.Collections;
using inv;
/// <summary>
/// Store
/// 1. Display equipments available to the player 
/// 2. Integrated and cooperate with inventory slots to display complete store.
/// 3. Change goods according to the level fo player.
/// </summary>
public class Store : MonoBehaviour
    {

        public int chaLevel;
        public inv.InventorySlots.item[] goods = new InventorySlots.item[7];
        public Texture2D[] icons;

        Vector2 windowSize = new Vector2(Screen.width, Screen.height * 1 / 3);
        Vector2 itemIconSize = new Vector2(Screen.width * 3 / 32, Screen.width * 3 / 32);
        Vector2 offSet = new Vector2(Screen.width * 3 / 384, Screen.width * 3 / 160);

        inv.InventorySlots.item checkedItem = new inv.InventorySlots.item();
        bool checking = false;

        Rect windowRect = new Rect();
        InventorySlots inventory;
        private GUIStyle info = new GUIStyle();

    // Use this for initialization
        void Start()
        {
            info.fontSize = Screen.width / 60;
            info.normal.textColor = Color.white;

            chaLevel = (int)Mathf.Sqrt(DBManager.GetPlayerEXP());
            checkedItem.type = "";
            windowRect = new Rect(0, 0, windowSize.x, windowSize.y);
            inventory = transform.GetComponent<InventorySlots>();

            if (inventory == null)
            {
                Debug.LogError("Store doesn't serve to any inventory");
            }

            putItems();
        }
        
        /// <summary>
        /// list the items in te store according to the level of player
        /// </summary>
        public void putItems()
        {
            string[] itemName = new string[7] { "Weapon", "Head", "Leg", "Feet", "Chest", "Potion", "Reviving" };

            for (int index = 0;index < goods.Length;index ++)
            {
                inv.InventorySlots.item temp = new inv.InventorySlots.item();

                temp.type = itemName[index];

                if (temp.type == "Potion" || temp.type == "Reviving")
                {
                    temp.maxAmount = 100;
                    if (temp.type == "Potion")
                    {
                        temp.price = 10;
                    }
                    else
                    {
                        temp.price = 50;
                    }
                    
                }
                else
                {
                    temp.level = chaLevel - chaLevel % 10;
                    temp.price = 1 + temp.level * 10;
                    temp.maxAmount = 1;
                    if (temp.type == "Weapon")
                    {
                        temp.attack = 5 + temp.level * 5;
                    }
                    else if (temp.type == "Head" || temp.type == "Leg" || temp.type == "Chest")
                    {                       
                        temp.armor = 10 + temp.level * 5;
                    }
                    else
                    {                    
                        temp.speed = 5 + temp.level * 5;
                    }
                }

                temp.amount = 1;
                goods[index] = temp;
            }
        }
        
        /// <summary>
        /// Manipulate selling an item to the player.
        /// </summary>
        /// <param name="item">an instance of item struct.</param>
        void sellItem(inv.InventorySlots.item item)
        {
            item.amount = 1;
            inventory.buyItem(item);
        }

        void OnGUI()
        {
            windowRect = GUI.Window(2, windowRect, displayStoreWindow, "Store");
        }

        void displayStoreWindow(int windowID)
        {
            float currentX = offSet.x;
            float currentY = 18 + offSet.y;
            int index = 0;

            //check button
            if (checking)
            {
                if (GUI.Button(new Rect(windowSize.x - itemIconSize.x * 2, 0, itemIconSize.x * 2, itemIconSize.y / 2 - 5), "Checking...."))
                {
                    checking = false;
                }
            }
            else
            {
                if (GUI.Button(new Rect(windowSize.x - itemIconSize.x * 2, 0, itemIconSize.x * 2, itemIconSize.y / 2 - 5), "Check Info"))
                {
                    checking = true;
                }
            }

            //item buttons
            foreach (inv.InventorySlots.item j in goods)
            {

                foreach (Texture2D t in icons)
                {
                    //something in the cell
                    if (t.name == j.type)
                    {
                        //render the icon for it
                        if (GUI.Button(new Rect(currentX, currentY, itemIconSize.x, itemIconSize.y), t))
                        {
                            if (checking)
                            {
                                checkedItem = j;
                            }
                            else
                            {
                                // sell the item if button gets pushed
                                if (inventory.hasSpace(j) && inventory.hasEnoughMoney(j))
                                {
                                    sellItem(j);
                                }
                            }

                        }
                    }
                }
                index++;
                currentX += itemIconSize.x;
            }

        //display info checked of an item
        if (checking)
        {

            GUI.Label(new Rect(currentX, 30, 200, 20), "Type: " + checkedItem.type.ToString(),info);
            GUI.Label(new Rect(currentX, 50, 200, 20), "Level: " + checkedItem.level.ToString(), info);
            GUI.Label(new Rect(currentX, 70, 200, 20), "Armor: " + checkedItem.armor.ToString(), info);
            GUI.Label(new Rect(currentX, 90, 200, 20), "Attack: " + checkedItem.attack.ToString(), info);
            GUI.Label(new Rect(currentX , 110, 200, 20), "Speed: " + checkedItem.speed.ToString(), info);
            GUI.Label(new Rect(currentX , 130, 200, 20), "Amount: " + checkedItem.amount.ToString(), info);
            GUI.Label(new Rect(currentX , 150, 200, 20), "MaxAmount: " + checkedItem.maxAmount.ToString(), info);
            GUI.Label(new Rect(currentX , 170, 200, 20), "Price: " + checkedItem.price.ToString(), info);

        }
    }

}






