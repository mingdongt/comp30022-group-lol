﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Interface for object who can get damaged.
/// </summary>
public interface IDamageable
{
	// Specifies how the object gets damaged.
	void TakeDamage (float damage);

	// Specifies when the object get downed.
	void TakeDown ();

	// Checks if the object is downed.
	bool IsDown ();
}
