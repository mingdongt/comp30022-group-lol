﻿using UnityEngine;
using System.Collections;

public abstract class Character : MonoBehaviour, IDamageable
{

	public float healthPoint = 100.0f;
	public float resistance = 0.5f;
	public float strength = 0.5f;
	public float attackInterval = 0.5f;
	public INetworkPlayer netPlayer;
	public  Weapon weapon;
	float nextAttack;

	public Animation anim;
	protected bool isDown;
	protected bool damaged;

    public int level;
    public int armor;
    public int attack;
    public int exp;
    public int gold;

	void Awake ()
	{
		Init ();
	}

	// Use this for initialization
	void Start () 
	{
		isDown = false;
		damaged = false;
		nextAttack = Time.time;
	}

 

    /// <summary>
    /// Calculates the damage to receive.
    /// </summary>
    /// <returns>The damage.</returns>
    /// <param name="damage">Damage.</param>
    protected float calcDamage (float damage)
	{
		return (1.0f - resistance) * damage;
	}

	public bool CanAttack ()
	{
		if (!IsDown () && !anim.IsPlaying ("Attack") && !anim.IsPlaying ("Damage") && !anim.IsPlaying ("Dead")) 
		{
			if (Time.time > nextAttack) 
			{
				nextAttack = Time.time + attackInterval;
				return true;
			}
		}
		return false;
	}

	public bool CanWalk ()
	{
		if (!IsDown () && !anim.IsPlaying ("Damage") && !anim.IsPlaying ("Dead"))
			return true;
		return false;
	}

	public bool CanTurn ()
	{
		if (!IsDown () && !anim.IsPlaying ("Attack") && !anim.IsPlaying ("Damage") && !anim.IsPlaying ("Dead"))
			return true;
		return false;
	}

	public bool CanWait ()
	{
		if (!IsDown () && !anim.IsPlaying ("Attack") && !anim.IsPlaying ("Damage") && !anim.IsPlaying ("Dead"))
			return true;
		return false;
	}

	/// <summary>
	/// Turns the isDown flag on thereby preventing the player from further actions.
	/// </summary>
	public void TakeDown ()
	{
		isDown = true;
		anim.Play ("Dead");
	}

    public void BringUp () {
        isDown = false;
    }
	public void Attack ()
	{
		weapon.Attack ();
	}

	public bool IsDown ()
	{
		return isDown;
	}

	public bool IsControllable ()
	{
		return !isDown;
	}

	public float GetHealthPoint ()
	{
		return healthPoint;
	}

	public float GetStrength ()
	{
		return strength;
	}

	public float GetResistance ()
	{
		return resistance;
	}

	public virtual void Init () 
	{
		// Gets Animation object.
		if ((anim = GetComponent<Animation> ()) == null)
			Debug.LogError ("The character needs an Animator.");

		// Gets Animation object.
		if ((weapon = GetComponent<Weapon> ()) == null)
			Debug.LogError ("The character needs a weapon.");
	}

	[PunRPC]
	public void PlayAnim (string animToPlay, PhotonMessageInfo info)
	{
		if (!info.photonView.isMine)
			anim.Play (animToPlay);
	}

	[PunRPC]
	public void TakeDamage (int photonId, float damage, PhotonMessageInfo info)
	{
		//Debug.Log ("TakeDamage: sender " + info.sender + "; ownerId " + info.photonView.ownerId + "; viewID " +info.photonView.viewID + "; photonId " +photonId);	
		PhotonView pview = PhotonView.Find (photonId);
		//PhotonView pview = info.photonView;
		if (!info.photonView.isMine)
			pview.gameObject.GetComponent<Character> ().TakeDamage (damage);
	}


	public abstract void TakeDamage (float damage);
}
