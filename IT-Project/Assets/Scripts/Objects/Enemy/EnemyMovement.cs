﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class is responsible for handling enemy movements. This includes
/// logic to evaluate what player to follow depending on which player is
/// closer. It also contains logic to identify when an enemy can attack and
/// when it cannot.
/// </summary>
public class EnemyMovement : MonoBehaviour 
{
	private Character player;
	private Character[] players;
	private Character enemy;
    private NavMeshAgent nav;
    private Animation anim;
	private Weapon weapon;
	private bool playerInRange = false;
	private float attackRange = 80f; // Within this range, enemy starts attacking.

    // Use this for initialization
    void Awake () 
	{       
		if ((nav = GetComponent<NavMeshAgent>()) == null)
			Debug.LogError("NavMeshAgent is not found.");

		if ((enemy = GetComponent<Character>()) == null)
			Debug.LogError("The character needs to be an enemy.");
		
		if ((anim = GetComponent<Animation>()) == null)
            Debug.LogError("The character needs an Animator.");
		
		if ((weapon = GetComponent<Weapon>()) == null)
			Debug.LogError("The character needs a weapon."); 

	}

	void Start ()
	{
		Init ();
	}

	private void Init ()
	{
		if (!GameManager.Instance.singleplay && !PhotonNetwork.isMasterClient)
			return;

		GameObject[] playerObjects = GameObject.FindGameObjectsWithTag ("Player");
		players = new Character[playerObjects.Length];
		for (int i = 0; i < playerObjects.Length; i++)
			players [i] = playerObjects [i].GetComponent<Character> ();
	}

	void Update () 
	{
		SetTarget ();
		Attack ();
    }

	// Set the closest non-downed player as the target.
	private void SetTarget ()
	{
		if (!GameManager.Instance.singleplay && !PhotonNetwork.isMasterClient)
			return;

		player = findClosestPlayer ();
		if (player != null)
			nav.SetDestination (player.transform.position);

	}

	// Attempt to attack the set target.
	private void Attack ()
	{
		if (playerInRange && !player.IsDown ())
			weapon.Attack ();

		if (!anim.IsPlaying ("Attack") && !anim.IsPlaying ("Dead") && !anim.IsPlaying ("Damage"))
			anim.Play ("Walk");
	}

	void OnPhotonPlayerDisconnected ()
	{
		Init ();
	}

	void OnJoinedRoom ()
	{
		Init ();
	}


	/// <summary>
	/// Finds the closest player.
	/// This method goes through a list of players currently in the game,
	/// and evaluates the distance between the enemy and the player, and which
	/// ever player is the closest to the enenmy, is the one that is followed/attacked
	/// by the enemy
	/// </summary>
	/// <returns>The closest player.</returns>
	Character findClosestPlayer() 
	{
		Character closestCharcter = players [0];
		float closestDistance = Vector3.Distance (closestCharcter.transform.position, transform.position);

		try 
		{
			for (int i = 0; i < players.Length; i++) 
			{
				float currentDistance = Vector3.Distance (players [i].transform.position, transform.position);
				if (closestDistance < currentDistance)
					currentDistance = closestDistance;

				if (!players[i].IsDown()) 
				{
					if (currentDistance < closestDistance) 
						closestCharcter = players [i];
				}
				if (currentDistance < attackRange)
					playerInRange = true;
				else
					playerInRange = false;
			}
		} catch (MissingReferenceException ex) {
			Debug.Log ("Enemy is looking for another target");
		}
		return closestCharcter;
	}

}