﻿using UnityEngine;
using System.Collections;

public class Enemy : Character 
{
    public EnemyManager enemyMangager;
    /// <summary>
    /// Reduces health point. When health point <= 0, calls TakeDown.
    /// </summary>
    /// <param name="damage">Damage value.</param>
    /// 

    public void Start() {

        initial();
        enemyMangager = GameObject.FindGameObjectWithTag("EnemyManager").GetComponent<EnemyManager>();

    }

    void initial()
    {
        armor = level;
        attack = level;
        gold = level;
        exp = level;
    }

	/// <summary>
	/// Reduces health point. When health point <= 0, calls TakeDown.
	/// </summary>
	/// <param name="damage">Damage value.</param>
	public override void TakeDamage (float damage)
	{
		//Debug.Log ("take damage called: " +damage);
		if (healthPoint < calcDamage (damage)) 
		{
			healthPoint = 0;
			if (isDown == false)
			{
				if (!GameManager.Instance.singleplay) 
				{
					netPlayer.IsDown = true;
					netPlayer.TakeDownRPC ();
				}
				TakeDown ();
				Destroy (gameObject, 1f);
			}
		} else {
			healthPoint -= calcDamage (damage);
			anim.Play ("Damage");
			damaged = true;
		}
	}

	[PunRPC]
	public void TakeDown (PhotonMessageInfo info)
	{
		if (!info.photonView.isMine) {
			TakeDown ();
			netPlayer.IsDown = true;
			Destroy (gameObject, 1f);
		}
	}
}
