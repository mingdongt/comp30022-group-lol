﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Enemy manager is responsible for spawning enemies, depending
/// on if the mode is multiplayer or singleplayer. It will also keep
/// track of the number of defeated enemies and the number of spawned
/// enemies.
/// </summary>
public class EnemyManager : MonoBehaviour {

    public Character player;
	public string  enemyPrefabName;
    public Transform spawnPoint;
    public int enemiesDefeated = 0;
    public int enemiesSpawned = 0;
	private GameObject manager;
	private bool singlePlay;
    private float spawnTime = 1f;
    private int waveSize = 10;
    public bool newWave;
    public int enemiesLevel = 0;
    private GameObject enemyTemp;
    private Texture textureTemp;

    private string[] textureName = new string[4] { "Cha_SlimBlue.tga", "Cha_SlimGreen.tga", "Cha_SlimRed.tga", "Cha_SlimYellow.tga" };

    // Use this for initialization
    void Start () {

		manager = GameObject.Find ("_Manager");
		singlePlay = manager.GetComponent<GameManager> ().singleplay;
        InvokeRepeating("Spawn", spawnTime, spawnTime);

		if (GameManager.Instance.singleplay)
			enemyPrefabName = GameManager.Instance.singleEnemyPrefabName;
		else
			enemyPrefabName = GameManager.Instance.multiEnemyPrefabName;
	}

    // Update is called once per frame
    void Spawn() {


        if (enemiesSpawned - enemiesDefeated ==0) {
            newWave = true;
            enemiesLevel += 1;
            //textureTemp = (Texture)AssetDatabase.LoadAssetAtPath("Assets/Actors/Monster/Mot_Slime/Textures/" + textureName[Random.Range(0,3)], typeof(Texture));
        }
        if (newWave)
        {
            if (!singlePlay)
                enemyTemp = PhotonNetwork.Instantiate(enemyPrefabName, spawnPoint.position, spawnPoint.rotation, 0) as GameObject;
            else
                enemyTemp = Instantiate(Resources.Load(enemyPrefabName), spawnPoint.position, spawnPoint.rotation) as GameObject;

            enemyTemp.GetComponent<Enemy>().level = enemiesLevel;
            //enemyTemp.transform.Find("ModelSlime").GetComponent<SkinnedMeshRenderer>().material.mainTexture = textureTemp;
            enemiesSpawned += 1;

        }
        if (enemiesSpawned % waveSize == 0)
        {
            newWave = false;
        }

    }

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {



	}
}
