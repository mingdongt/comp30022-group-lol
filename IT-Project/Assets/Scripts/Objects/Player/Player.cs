﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : Character 
{
	private const float MAX_HEALTH = 100f;
	public Slider healthSlider;
	public Image damageImage;
	public Color flashcolour = new Color(1f, 0f, 0f, 0.1f);
	public float flashSpeed = 5.0f;
    public int speed;
    private CharacterSlots slots;
    private EnemyManager enemyManager;

    void Start()
    {
        
        enemyManager = GameObject.Find("EnemyManager").GetComponent<EnemyManager>();
        slots = GameObject.Find("characterTemp").GetComponent<CharacterSlots>();
        initial();
    }

    void initial()
    {
        
        // Attributes
        slots.calculateAttributes();
        attack = slots.attack;
        armor = slots.armor;
        speed = slots.speed;
        Debug.Log(attack);
        Debug.Log(speed);
        // accumulatd gold and exp got from killing monster
        gold = 0;
        exp = 0;
    }



    public void Update ()
	{
		if (damaged) 
			damageImage.color = flashcolour;
		else
			damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);

		damaged = false;
	}
		
	// Player & UI initialisation.
	public override void Init ()
	{
		if (GameManager.Instance.singleplay)
		{
			anim = GetComponent <Animation> ();
			weapon = GetComponent<Weapon> ();
			// Delegates the initialisation of the scene.
			GameManager.Instance.ScManager.InitScene (this);
		}
	}

	/// <summary>
	/// Reduces health point. When health point <= 0, calls TakeDown.
	/// </summary>
	/// <param name="damage">Damage value.</param>
	public override void TakeDamage (float damage)
	{
		// Shouldn't take damage if already down.
		if (isDown)
			return;
				
		//Debug.Log ("take damage called: " +damage);
		if (healthPoint < calcDamage (damage)) 
		{
			healthPoint = 0;
			if (!isDown)
			{
				if (!GameManager.Instance.singleplay) {
					netPlayer.IsDown = true;
					netPlayer.TakeDownRPC ();
				}

				TakeDown ();
			}
		} 
		else 
		{
			healthPoint -= calcDamage (damage);
			if (GameManager.Instance.singleplay || !GameManager.Instance.singleplay && netPlayer.GetPhotonView().isMine)
				healthSlider.value = healthPoint;
			anim.Play ("Damage");
			damaged = true;
		}
	}

	[PunRPC]
	public void TakeDown (PhotonMessageInfo info)
	{
		
		if(!GameManager.Instance.singleplay && info.photonView.isMine)
			netPlayer.DisplayReviveRPC (GetComponent<PhotonView> ().viewID);
		
		if (!info.photonView.isMine)
		{
			TakeDown ();
			netPlayer.IsDown = true;
		}
	}


	/// <summary>
	/// Displays the revive button on the player that is down
	/// on the client's screen.
	/// </summary>
	/// <param name="photonId">Photon identifier.</param>
	/// <param name="info">Info.</param>
	[PunRPC]
	public void DisplayRevive(int photonId, PhotonMessageInfo info) {

		PhotonView pview = PhotonView.Find (photonId);

		if (!info.photonView.isMine) {

			pview.gameObject.GetComponent<Reviving> ().enabled = true;
		}

	}

	/// <summary>
	/// Sets the parameters once the player is revived.
	/// when the player is revived it is spawned to the
	/// wait animation, with half of the max health.
	/// </summary>
	[PunRPC]
	public void Revived() {

		netPlayer.IsDown = false;
		this.isDown = false;
		netPlayer.PlayAnimRPC ("Wait");
		this.healthPoint = MAX_HEALTH / 2;


	}


	/// <summary>
	/// This tells other clients that somebody is currently trying to
	/// revive a downed person, so to avoid two people trying to press
	/// the same button.
	/// </summary>
	/// <param name="photonId">Photon identifier.</param>
	/// <param name="gettingRevived">If set to <c>true</c> getting revived.</param>
	/// <param name="info">Info.</param>
	[PunRPC]
	public void GettingRevived(int photonId,bool gettingRevived, PhotonMessageInfo info) {

		PhotonView pview = PhotonView.Find (photonId);

		if(!info.photonView.isMine) 
			pview.gameObject.GetComponent<Reviving> ().GettingRevived = gettingRevived;

	}

	/// <summary>
	/// Resets the revived status of the player getting getting revived,
	/// basically if somebody does not completely revive a person, we 
	/// need to reset the 'reviving' flag to display the button back to
	/// other clients.
	/// </summary>
	/// <param name="photonId">Photon identifier.</param>
	/// <param name="info">Info.</param>
	[PunRPC]
	public void ResetRevive(int photonId, PhotonMessageInfo info) {

		PhotonView pview = PhotonView.Find (photonId);

		if (!info.photonView.isMine) {
			pview.gameObject.GetComponent<Reviving> ().Revived = false;
			pview.gameObject.GetComponent<Reviving> ().enabled = false;
		}
	}

}
