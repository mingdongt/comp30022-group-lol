﻿using UnityEngine;
using System.Collections;

public class SkillBoom : MonoBehaviour
{

    ParticleSystem hitParticles;

    public float damage;
    public void Start()
    {
        hitParticles = transform.Find("SkillBoom").GetComponent<ParticleSystem>();
    }

	[PunRPC]
	public void UseSkill (PhotonMessageInfo info)
	{
		if (!info.photonView.isMine)
			cast (); 
	}

	public void cast()
    {
        hitParticles.transform.position = transform.position;
        hitParticles.Play();

        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");

        foreach (GameObject target in enemies)
        {
            float distance = Vector3.Distance(target.transform.position, transform.position);
            if (distance < 250)
            {
                IDamageable opp = target.transform.GetComponent<IDamageable>();
                opp.TakeDamage(damage);
                target.transform.position += target.transform.position - transform.position;

            }
        }

        

        

    }


}
