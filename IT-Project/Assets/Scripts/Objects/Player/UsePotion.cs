﻿using UnityEngine;
using System.Collections;

public class UsePotion : MonoBehaviour {

    ParticleSystem effect;
    public float HP;
	// Use this for initialization
	public void Start () {

        effect = transform.Find("PotionEffect").GetComponent<ParticleSystem>();
    }

	[PunRPC]
	public void UsingPotion (PhotonMessageInfo info)
	{
		if (!info.photonView.isMine)
			Use ();
	}

	public void Use()
    {
		
        effect.transform.position = transform.position;
        effect.Play();

        transform.GetComponent<Player>().healthPoint += HP;

        if (transform.GetComponent<Player>().healthPoint > 100)
        {
            transform.GetComponent<Player>().healthPoint = 100;
        }
       
    }


}
