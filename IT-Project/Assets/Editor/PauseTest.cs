﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEditor;
using NUnit.Framework;
using NSubstitute;
using System;

public class PauseTest {

	[Test]
	public void PauseOnPointerDownTest_1()
	{
        //Arrange
        var pauseControl = new PauseButton();

        //unity's stupid thing with draggin images in inspector
        try {
            pauseControl.pause();
        }
        catch(NullReferenceException e) {

        }
        //Assert
        //starts false, after one pause, should be true
        Assert.AreEqual(pauseControl.paused, true);

    }

    [Test]
    public void PauseOnPointerDownTest_2() {
        //Arrange
        var pauseControl = new PauseButton();
        //unity's stupid thing with draggin images in inspector
        try {
            pauseControl.pause();
        }
        catch (NullReferenceException e) {
        }

        try {
            pauseControl.pause();
        }
        catch (NullReferenceException e) {
        }
        //Assert
        //starts false, after two calls, should be back to false
        Assert.AreEqual(pauseControl.paused, false);

    }
}
