﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEditor;
using NUnit.Framework;
using NSubstitute;
using System;

[TestFixture]
public class HPTest {

	[Test]
	public void TakeDamage_Test()
	{
		//setup
		var player = new Player ();


		try {
			//call function
			player.TakeDamage(20f);
		}
		catch(NullReferenceException e) {
			
		}

		//assert
		Assert.AreEqual(player.healthPoint,90f);
	}
}
