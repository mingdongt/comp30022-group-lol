# Knight vs Slime

# Group Information

## Members

* Masaki Goto
* Mingdong Tan
* Mylan Li
* Rishabh Sharma


# Project Information

## Where can the code be found?
* IT-Project/Assets/Scripts

## What Testing modules were used?
* Unity Test Tools, NSubtitute, which can be found by navigating to IT-Project/Assets/UnityTestTools

## Where can the Tests be found?
* The tests can be found in IT-Project/Assets/Scripts/Tests

## What was our code review guide-line?
https://itproject-squad.slack.com/files/rish.sharma/F2DNLUA57/Code_Review_Criteria